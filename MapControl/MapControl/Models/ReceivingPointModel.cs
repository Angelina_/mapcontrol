﻿using KirasaModelsDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfMapControl;

namespace MapControl
{
    public class ReceivingPointModel : TableReceivingPoint
    {
        public IMapObject MapObject { get; set; }

        public ReceivingPointModel(IMapObject mapObject)
        {
            MapObject = mapObject;
            
        }
    }
}
