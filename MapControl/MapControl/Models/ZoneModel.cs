﻿using Mapsui.Providers;
using System;
using System.Windows;
using System.Windows.Controls;
using WpfMapControl;

namespace MapControl
{
    public class ZoneModel : ZoneParams
    {
        public IFeature Zone { get; set; }

      
        public ZoneModel(IFeature zone)
        {
            Zone = zone;
        }
    }
}
