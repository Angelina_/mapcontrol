﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapControl
{
    public class EllipseParams
    {
        public short Head { get; set; }
        public float RadiusA { get; set; }
        public float RadiusB { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }

        public EllipseParams()
        {

        }
    }
}
