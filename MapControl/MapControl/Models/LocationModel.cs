﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using WpfMapControl;

namespace MapControl
{
    public class LocationModel : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        #endregion

        private Location coordinate;
        public Location Coordinate
        {
            get => coordinate;
            set
            {
                coordinate = value;
                OnPropertyChanged();
            }
        }

        public Location ClickCoord { get; set; }

        public LocationModel()
        {
            Coordinate = new Location();
        }

    }
}
