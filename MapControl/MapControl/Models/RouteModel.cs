﻿using Mapsui.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfMapControl;

namespace MapControl
{
    public class RouteModel
    {
        public Feature FeatureRoute { get; set; }

        public RouteModel(Feature distance)
        {
            FeatureRoute = distance;
        }
    }
}
