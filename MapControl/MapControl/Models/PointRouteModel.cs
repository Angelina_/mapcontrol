﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfMapControl;

namespace MapControl
{
    public class PointRouteModel
    { 
        public IMapObject MapObject { get; set; }
   
        public PointRouteModel(IMapObject mapObject)
        {
            MapObject = mapObject;
        }
    }
  
}
