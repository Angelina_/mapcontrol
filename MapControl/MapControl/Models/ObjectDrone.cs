﻿using KirasaModelsDBLib;
using Mapsui.Providers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfMapControl;

namespace MapControl
{
    public class ObjectDrone 
    {
        public List<TableTrajectory> Trajectories { get; set; }
        public TableUAVBase UAVRes { get; set; }

        public ObjectDrone() : base()
        {
           
        }
    }

    public class ObjectDroneAero
    {
        public List<TableAeroscopeTrajectory> Trajectories { get; set; }
        public TableAeroscope UAVRes { get; set; }

        public ObjectDroneAero() : base()
        {

        }
    }
}
