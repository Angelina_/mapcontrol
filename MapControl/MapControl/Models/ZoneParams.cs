﻿using Mapsui.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapControl
{
    //public class Zones 
    //{
    //    public Feature Zone { get; set; }

    //    public Zones(Feature zone)
    //    {
    //        Zone = zone;
    //    }
    //}

    public class ZoneParams
    {
        public short Radius { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        //public Feature Zone { get; set; }

        public ZoneParams()
        {
          
        }
        //public ZoneParams(short radius, double latitude, double longitude)
        //{
        //    Radius = radius;
        //    Latitude = latitude;
        //    Longitude = longitude;
        //}
    }
}
