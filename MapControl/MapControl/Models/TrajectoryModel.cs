﻿using KirasaModelsDBLib;
using Mapsui.Providers;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using WpfMapControl;

namespace MapControl
{
    public class TrajectoryModel : ObjectDrone
    {
        public IMapObject MapObject { get; set; }
        public Feature MapTrack { get; set; }

        public TrajectoryModel(IMapObject mapObject, Feature mapTrack)
        {
            MapObject = mapObject;
            MapTrack = mapTrack;
        }
    }

    public class TrajectoryAeroscopeModel : ObjectDroneAero
    {
        public IMapObject MapObject { get; set; }
        public Feature MapTrack { get; set; }

        public TrajectoryAeroscopeModel(IMapObject mapObject, Feature mapTrack)
        {
            MapObject = mapObject;
            MapTrack = mapTrack;
        }
    }


}
