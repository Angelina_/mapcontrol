﻿using KirasaModelsDBLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Threading;
using WpfMapControl;

namespace MapControl
{
    public partial class UserControlMap : UserControl
    {
        //public byte Projection { get; set; } = 2;

        //public const double distanse = 1500;

        #region LocalReceivingPoint
        private List<TableReceivingPoint> localReceivingPoint;
        public List<TableReceivingPoint> LocalReceivingPoint
        {
            get => localReceivingPoint;
            set
            {
                //if (localReceivingPoint != null && localReceivingPoint.Equals(value)) return;
                localReceivingPoint = value;
                UpdateLocalReceivingPoint();
            }
        }
        private void UpdateLocalReceivingPoint()
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                if (((MapViewModel)DataContext).LocalReceivingPointModel != null)
                {
                    for (int i = 0; i < ((MapViewModel)DataContext).LocalReceivingPointModel.Count; i++)
                    {
                        mapControl.RemoveObject(((MapViewModel)DataContext).LocalReceivingPointModel[i].MapObject);
                    }
                }
                ((MapViewModel)DataContext).LocalReceivingPointModel = new ObservableCollection<ReceivingPointModel> { };

                foreach (var receivingPoint in LocalReceivingPoint)
                {
                    if (receivingPoint.IsCetral)
                    {
                        ((MapViewModel)DataContext).LocalReceivingPointModel.Add(new ReceivingPointModel(DrawObject(receivingLocalPointOwnObjectStyle,
                            new Location(receivingPoint.Coordinates.Longitude, receivingPoint.Coordinates.Latitude, receivingPoint.Coordinates.Altitude))));
                    }
                    else
                    {
                        ((MapViewModel)DataContext).LocalReceivingPointModel.Add(new ReceivingPointModel(DrawObject(receivingLocalPointObjectStyle, receivingPoint.Id.ToString(),
                            new Location(receivingPoint.Coordinates.Longitude, receivingPoint.Coordinates.Latitude, receivingPoint.Coordinates.Altitude))));
                    }
                }
            }), DispatcherPriority.Background);
        }
        #endregion

        #region RemoteReceivingPoint
        private List<TableReceivingPoint> remoteReceivingPoint;
        public List<TableReceivingPoint> RemoteReceivingPoint
        {
            get => remoteReceivingPoint;
            set
            {
                //if (remoteReceivingPoint != null && remoteReceivingPoint.Equals(value)) return;
                remoteReceivingPoint = value;
                UpdateRemoteReceivingPoint();
            }
        }
        private void UpdateRemoteReceivingPoint()
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                if (((MapViewModel)DataContext).RemoteReceivingPointModel != null)
                {
                    for (int i = 0; i < ((MapViewModel)DataContext).RemoteReceivingPointModel.Count; i++)
                    {
                        mapControl.RemoveObject(((MapViewModel)DataContext).RemoteReceivingPointModel[i].MapObject);
                    }
                }
                ((MapViewModel)DataContext).RemoteReceivingPointModel = new ObservableCollection<ReceivingPointModel> { };

                foreach (var receivingPoint in RemoteReceivingPoint)
                {
                    if (receivingPoint.IsCetral)
                    {
                        ((MapViewModel)DataContext).RemoteReceivingPointModel.Add(new ReceivingPointModel(DrawObject(receivingRemotePointOwnObjectStyle,
                            new Location(receivingPoint.Coordinates.Longitude, receivingPoint.Coordinates.Latitude, receivingPoint.Coordinates.Altitude))));
                    }
                    else
                    {
                        ((MapViewModel)DataContext).RemoteReceivingPointModel.Add(new ReceivingPointModel(DrawObject(receivingRemotePointObjectStyle, receivingPoint.Id.ToString(),
                            new Location(receivingPoint.Coordinates.Longitude, receivingPoint.Coordinates.Latitude, receivingPoint.Coordinates.Altitude))));
                    }
                }
            }), DispatcherPriority.Background);
        }
        #endregion

        #region Zones 
        private List<ZoneParams> zoneParams;
        public List<ZoneParams> ZoneParams
        {
            get => zoneParams;
            set
            {
                if (zoneParams != null && zoneParams.Equals(value)) return;
                zoneParams = value;
                UpdateZone();
            }
        }
        private void UpdateZone()
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                if (((MapViewModel)DataContext).ZoneModel != null)
                {
                    for (int i = 0; i < ((MapViewModel)DataContext).ZoneModel.Count; i++)
                    {
                        mapControl.RemoveObject(((MapViewModel)DataContext).ZoneModel[i].Zone);
                    }
                }

                ((MapViewModel)DataContext).ZoneModel = new ObservableCollection<ZoneModel> { };

                if (ZoneParams.Count > 0)
                {
                    for (int i = 0; i < ZoneParams.Count; i++)
                    {
                        List<Location> ListCoord = new List<Location>(360);
                        for (int j = 0; j < 360; j++)
                        {
                            ListCoord.Add(DefinePointSphere(new Location(ZoneParams[i].Latitude, ZoneParams[i].Longitude), j, ZoneParams[i].Radius));
                        }

                        switch(i)
                        {
                            case 0:
                                ((MapViewModel)DataContext).ZoneModel.Add(new ZoneModel(DrawFeaturePolygon(TypeFeature.Polygon, ListCoord, BrushZoneAlarm, PenZoneAlarm, 1)));
                                break;

                            case 1:
                                ((MapViewModel)DataContext).ZoneModel.Add(new ZoneModel(DrawFeaturePolygon(TypeFeature.Polygon, ListCoord, BrushZoneAttention, PenZoneAttention, 1)));
                                break;

                            default:
                                break;
                        }
                    }
                }

               
            }), DispatcherPriority.Background);
        }
        #endregion

        #region TrajectoryUAVRes
        private List<ObjectDrone> trajectoryUAVRes;
        public List<ObjectDrone> TrajectoryUAVRes
        {
            get => trajectoryUAVRes;
            set
            {
                if (trajectoryUAVRes != null && trajectoryUAVRes.Equals(value)) return;
                trajectoryUAVRes = value;
                UpdateTrajectoryUAVRes();
            }
        }
        private void UpdateTrajectoryUAVRes()
        {
            try
            {
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    if (((MapViewModel)DataContext).TrajectoryModel != null)
                    {
                        for (int i = 0; i < ((MapViewModel)DataContext).TrajectoryModel.Count; i++)
                        {
                            mapControl.RemoveObject(((MapViewModel)DataContext).TrajectoryModel[i].MapObject);
                            mapControl.RemoveObject(((MapViewModel)DataContext).TrajectoryModel[i].MapTrack);
                        }
                    }
                    if (TrajectoryUAVRes.Count > 0)
                    {
                        ((MapViewModel)DataContext).TrajectoryModel = new ObservableCollection<TrajectoryModel> { };

                        //ObjectDronesUAVRes = new List<ObjectDrone>(TrajectoryUAVRes.Count);
                        tempLastPointTrack = new List<TableTrajectory>();

                        foreach (var trajectory in TrajectoryUAVRes)
                        {
                            if (trajectory.Trajectories.Count > 0)
                            {
                                int countPoints = 0;
                                double dist = 0;
                                for (int i = trajectory.Trajectories.Count - 1; i > 0; i--)
                                {
                                    double dist2Points = Bearing.ClassBearing.f_D_2Points(
                                        trajectory.Trajectories[i].Coordinates.Latitude,
                                        trajectory.Trajectories[i].Coordinates.Longitude,
                                        trajectory.Trajectories[i - 1].Coordinates.Latitude,
                                        trajectory.Trajectories[i - 1].Coordinates.Longitude, 1);

                                    dist += dist2Points;
                                    if (dist < LengthOfTrack)
                                    {
                                        countPoints += 1;
                                    }
                                    else break;
                                }

                                int count = trajectory.Trajectories.Count - countPoints;

                                IEnumerable<TableTrajectory> table = trajectory.Trajectories.Skip(count);

                                // Записать последнюю точку траектории для строба
                                //if (table.Count() > 0)
                                //{
                                //    tempLastPointTrack.Add(table.ToList().LastOrDefault());
                                //    LastPointTrack = tempLastPointTrack;
                                //}
                                //-----------------------------------------------

                                List<Location> locationTrack = new List<Location>(trajectory.Trajectories.Count - count);

                                foreach (var track in table)
                                    locationTrack.Add(new Location(track.Coordinates.Latitude, track.Coordinates.Longitude, track.Coordinates.Altitude));

                                switch (trajectory.UAVRes.Id)
                                {
                                    case 0:
                                        ((MapViewModel)DataContext).TrajectoryModel.Add(new TrajectoryModel(DrawObject(droneUAVResId1ObjectStyle, ConvertTypeDroneUAVRes(trajectory.UAVRes.Type),
                                                new Location(trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Longitude,
                                                             trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Latitude,
                                                             trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Altitude)),
                                                DrawFeaturePolygon(TypeFeature.Polyline, locationTrack, BrushTrackUAV_Id1, PenTrackUAV_Id1, 1)));
                                       
                                        tempLastPointTrack.Add(table.ToList().LastOrDefault());

                                        break;

                                    case 1:
                                        ((MapViewModel)DataContext).TrajectoryModel.Add(new TrajectoryModel(DrawObject(droneUAVResId2ObjectStyle, ConvertTypeDroneUAVRes(trajectory.UAVRes.Type),
                                                new Location(trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Longitude,
                                                             trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Latitude,
                                                             trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Altitude)),
                                                DrawFeaturePolygon(TypeFeature.Polyline, locationTrack, BrushTrackUAV_Id2, PenTrackUAV_Id2, 1)));

                                        tempLastPointTrack.Add(table.ToList().LastOrDefault());

                                        break;

                                    case 2:
                                        ((MapViewModel)DataContext).TrajectoryModel.Add(new TrajectoryModel(DrawObject(droneUAVResId3ObjectStyle, ConvertTypeDroneUAVRes(trajectory.UAVRes.Type),
                                                new Location(trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Longitude,
                                                             trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Latitude,
                                                             trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Altitude)),
                                                DrawFeaturePolygon(TypeFeature.Polyline, locationTrack, BrushTrackUAV_Id3, PenTrackUAV_Id3, 1)));
                                       
                                        tempLastPointTrack.Add(table.ToList().LastOrDefault());

                                        break;

                                    case 3:
                                        ((MapViewModel)DataContext).TrajectoryModel.Add(new TrajectoryModel(DrawObject(droneUAVResId4ObjectStyle, ConvertTypeDroneUAVRes(trajectory.UAVRes.Type),
                                                new Location(trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Longitude,
                                                             trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Latitude,
                                                             trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Altitude)),
                                                DrawFeaturePolygon(TypeFeature.Polyline, locationTrack, BrushTrackUAV_Id4, PenTrackUAV_Id4, 1)));

                                        tempLastPointTrack.Add(table.ToList().LastOrDefault());

                                        break;

                                    case 4:
                                        ((MapViewModel)DataContext).TrajectoryModel.Add(new TrajectoryModel(DrawObject(droneUAVResId5ObjectStyle, ConvertTypeDroneUAVRes(trajectory.UAVRes.Type),
                                                new Location(trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Longitude,
                                                             trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Latitude,
                                                             trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Altitude)),
                                                DrawFeaturePolygon(TypeFeature.Polyline, locationTrack, BrushTrackUAV_Id5, PenTrackUAV_Id5, 1)));

                                        tempLastPointTrack.Add(table.ToList().LastOrDefault());

                                        break;

                                    case 5:
                                        ((MapViewModel)DataContext).TrajectoryModel.Add(new TrajectoryModel(DrawObject(droneUAVResId6ObjectStyle, ConvertTypeDroneUAVRes(trajectory.UAVRes.Type),
                                                new Location(trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Longitude,
                                                             trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Latitude,
                                                             trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Altitude)),
                                                DrawFeaturePolygon(TypeFeature.Polyline, locationTrack, BrushTrackUAV_Id6, PenTrackUAV_Id6, 1)));

                                        tempLastPointTrack.Add(table.ToList().LastOrDefault());

                                        break;

                                    case 6:
                                        ((MapViewModel)DataContext).TrajectoryModel.Add(new TrajectoryModel(DrawObject(droneUAVResId7ObjectStyle, ConvertTypeDroneUAVRes(trajectory.UAVRes.Type),
                                                new Location(trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Longitude,
                                                             trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Latitude,
                                                             trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Altitude)),
                                                DrawFeaturePolygon(TypeFeature.Polyline, locationTrack, BrushTrackUAV_Id7, PenTrackUAV_Id7, 1)));

                                        tempLastPointTrack.Add(table.ToList().LastOrDefault());

                                        break;

                                    case 7:
                                        ((MapViewModel)DataContext).TrajectoryModel.Add(new TrajectoryModel(DrawObject(droneUAVResId8ObjectStyle, ConvertTypeDroneUAVRes(trajectory.UAVRes.Type),
                                                new Location(trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Longitude,
                                                             trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Latitude,
                                                             trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Altitude)),
                                                DrawFeaturePolygon(TypeFeature.Polyline, locationTrack, BrushTrackUAV_Id8, PenTrackUAV_Id8, 1)));

                                        tempLastPointTrack.Add(table.ToList().LastOrDefault());

                                        break;

                                    case 8:
                                        ((MapViewModel)DataContext).TrajectoryModel.Add(new TrajectoryModel(DrawObject(droneUAVResId9ObjectStyle, ConvertTypeDroneUAVRes(trajectory.UAVRes.Type),
                                                new Location(trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Longitude,
                                                             trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Latitude,
                                                             trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Altitude)),
                                                DrawFeaturePolygon(TypeFeature.Polyline, locationTrack, BrushTrackUAV_Id9, PenTrackUAV_Id9, 1)));

                                        tempLastPointTrack.Add(table.ToList().LastOrDefault());

                                        break;

                                    case 9:
                                        ((MapViewModel)DataContext).TrajectoryModel.Add(new TrajectoryModel(DrawObject(droneUAVResId10ObjectStyle, ConvertTypeDroneUAVRes(trajectory.UAVRes.Type),
                                                new Location(trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Longitude,
                                                             trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Latitude,
                                                             trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Altitude)),
                                                DrawFeaturePolygon(TypeFeature.Polyline, locationTrack, BrushTrackUAV_Id10, PenTrackUAV_Id10, 1)));

                                        tempLastPointTrack.Add(table.ToList().LastOrDefault());

                                        break;

                                    default:
                                        ((MapViewModel)DataContext).TrajectoryModel.Add(new TrajectoryModel(DrawObject(droneUAVResId1ObjectStyle, ConvertTypeDroneUAVRes(trajectory.UAVRes.Type),
                                                new Location(trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Longitude,
                                                             trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Latitude,
                                                             trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Altitude)),
                                                DrawFeaturePolygon(TypeFeature.Polyline, locationTrack, BrushTrackUAV_Id1, PenTrackUAV_Id1, 1)));

                                        tempLastPointTrack.Add(table.ToList().LastOrDefault());

                                        break;
                                }
                            }
                        }

                        for (int i = 0; i < tempLastPointTrack.Count; i++)
                        {
                            if (tempLastPointTrack[i] == null)
                            {
                                tempLastPointTrack.RemoveAt(i);
                                i--;
                            }
                        }
                        LastPointTrack = tempLastPointTrack;
                    }
                }), DispatcherPriority.Background);
            }
            catch { }
        }
        #endregion

        #region TrajectoryUAVResArchive
        private List<ObjectDrone> trajectoryUAVResArchive;
        public List<ObjectDrone> TrajectoryUAVResArchive
        {
            get => trajectoryUAVResArchive;
            set
            {
                //if (trajectoryUAVResArchive != null && trajectoryUAVResArchive.Equals(value)) return;
                trajectoryUAVResArchive = value;
                DisplayTrajectoryUAVResArchive();
            }
        }
        private void DisplayTrajectoryUAVResArchive()
        {
            try
            {
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    if (((MapViewModel)DataContext).TrajectoryArchiveModel != null)
                    {
                        for (int i = 0; i < ((MapViewModel)DataContext).TrajectoryArchiveModel.Count; i++)
                        {
                            mapControl.RemoveObject(((MapViewModel)DataContext).TrajectoryArchiveModel[i].MapObject);
                            mapControl.RemoveObject(((MapViewModel)DataContext).TrajectoryArchiveModel[i].MapTrack);
                        }
                    }

                    if (TrajectoryUAVResArchive != null)
                    {
                        if (TrajectoryUAVResArchive.Count > 0)
                        {
                            ((MapViewModel)DataContext).TrajectoryArchiveModel = new ObservableCollection<TrajectoryModel> { };

                            foreach (var trajectory in TrajectoryUAVResArchive)
                            {
                                if (trajectory.Trajectories.Count > 0)
                                {
                                    int countPoints = 0;
                                    double dist = 0;
                                    for (int i = trajectory.Trajectories.Count - 1; i > 0; i--)
                                    {
                                        double dist2Points = Bearing.ClassBearing.f_D_2Points(
                                            trajectory.Trajectories[i].Coordinates.Latitude,
                                            trajectory.Trajectories[i].Coordinates.Longitude,
                                            trajectory.Trajectories[i - 1].Coordinates.Latitude,
                                            trajectory.Trajectories[i - 1].Coordinates.Longitude, 1);

                                        dist += dist2Points;
                                        if (dist < LengthOfTrack)
                                        {
                                            countPoints += 1;
                                        }
                                        else break;
                                    }

                                    int count = trajectory.Trajectories.Count - countPoints;

                                    IEnumerable<TableTrajectory> table = trajectory.Trajectories.Skip(count);
                                    List<Location> locationTrack = new List<Location>(trajectory.Trajectories.Count - count);

                                    foreach (var track in table)
                                        locationTrack.Add(new Location(track.Coordinates.Latitude, track.Coordinates.Longitude, track.Coordinates.Altitude));


                                    ///////////////////////////
                                    switch (trajectory.UAVRes.Id)
                                    {
                                        case 0:
                                            ((MapViewModel)DataContext).TrajectoryArchiveModel.Add(new TrajectoryModel(DrawObject(droneUAVResArchiveId1ObjectStyle, ConvertTypeDroneUAVRes(trajectory.UAVRes.Type),
                                                    new Location(trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Longitude,
                                                                 trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Latitude,
                                                                 trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Altitude)),
                                                   DrawFeaturePolyline(locationTrack, PenTrackUAV_Id1)));
                                            break;

                                        case 1:
                                            ((MapViewModel)DataContext).TrajectoryArchiveModel.Add(new TrajectoryModel(DrawObject(droneUAVResArchiveId2ObjectStyle, ConvertTypeDroneUAVRes(trajectory.UAVRes.Type),
                                                    new Location(trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Longitude,
                                                                 trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Latitude,
                                                                 trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Altitude)),
                                                    DrawFeaturePolyline(locationTrack, PenTrackUAV_Id2)));
                                            break;

                                        case 2:
                                            ((MapViewModel)DataContext).TrajectoryArchiveModel.Add(new TrajectoryModel(DrawObject(droneUAVResArchiveId3ObjectStyle, ConvertTypeDroneUAVRes(trajectory.UAVRes.Type),
                                                    new Location(trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Longitude,
                                                                 trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Latitude,
                                                                 trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Altitude)),
                                                    DrawFeaturePolyline(locationTrack, PenTrackUAV_Id3)));
                                            break;

                                        case 3:
                                            ((MapViewModel)DataContext).TrajectoryArchiveModel.Add(new TrajectoryModel(DrawObject(droneUAVResArchiveId4ObjectStyle, ConvertTypeDroneUAVRes(trajectory.UAVRes.Type),
                                                    new Location(trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Longitude,
                                                                 trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Latitude,
                                                                 trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Altitude)),
                                                    DrawFeaturePolyline(locationTrack, PenTrackUAV_Id4)));
                                            break;

                                        case 4:
                                            ((MapViewModel)DataContext).TrajectoryArchiveModel.Add(new TrajectoryModel(DrawObject(droneUAVResArchiveId5ObjectStyle, ConvertTypeDroneUAVRes(trajectory.UAVRes.Type),
                                                    new Location(trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Longitude,
                                                                 trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Latitude,
                                                                 trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Altitude)),
                                                    DrawFeaturePolyline(locationTrack, PenTrackUAV_Id5)));
                                            break;

                                        case 5:
                                            ((MapViewModel)DataContext).TrajectoryArchiveModel.Add(new TrajectoryModel(DrawObject(droneUAVResArchiveId6ObjectStyle, ConvertTypeDroneUAVRes(trajectory.UAVRes.Type),
                                                    new Location(trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Longitude,
                                                                 trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Latitude,
                                                                 trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Altitude)),
                                                    DrawFeaturePolyline(locationTrack, PenTrackUAV_Id6)));
                                            break;

                                        case 6:
                                            ((MapViewModel)DataContext).TrajectoryArchiveModel.Add(new TrajectoryModel(DrawObject(droneUAVResArchiveId7ObjectStyle, ConvertTypeDroneUAVRes(trajectory.UAVRes.Type),
                                                    new Location(trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Longitude,
                                                                 trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Latitude,
                                                                 trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Altitude)),
                                                    DrawFeaturePolyline(locationTrack, PenTrackUAV_Id7)));
                                            break;

                                        case 7:
                                            ((MapViewModel)DataContext).TrajectoryArchiveModel.Add(new TrajectoryModel(DrawObject(droneUAVResArchiveId8ObjectStyle, ConvertTypeDroneUAVRes(trajectory.UAVRes.Type),
                                                    new Location(trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Longitude,
                                                                 trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Latitude,
                                                                 trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Altitude)),
                                                    DrawFeaturePolyline(locationTrack, PenTrackUAV_Id8)));
                                            break;

                                        case 8:
                                            ((MapViewModel)DataContext).TrajectoryArchiveModel.Add(new TrajectoryModel(DrawObject(droneUAVResArchiveId9ObjectStyle, ConvertTypeDroneUAVRes(trajectory.UAVRes.Type),
                                                    new Location(trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Longitude,
                                                                 trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Latitude,
                                                                 trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Altitude)),
                                                    DrawFeaturePolyline(locationTrack, PenTrackUAV_Id9)));
                                            break;

                                        case 9:
                                            ((MapViewModel)DataContext).TrajectoryArchiveModel.Add(new TrajectoryModel(DrawObject(droneUAVResArchiveId10ObjectStyle, ConvertTypeDroneUAVRes(trajectory.UAVRes.Type),
                                                    new Location(trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Longitude,
                                                                 trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Latitude,
                                                                 trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Altitude)),
                                                    DrawFeaturePolyline(locationTrack, PenTrackUAV_Id10)));
                                            break;

                                        default:
                                            ((MapViewModel)DataContext).TrajectoryArchiveModel.Add(new TrajectoryModel(DrawObject(droneUAVResArchiveId1ObjectStyle, ConvertTypeDroneUAVRes(trajectory.UAVRes.Type),
                                                    new Location(trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Longitude,
                                                                 trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Latitude,
                                                                 trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Altitude)),
                                                    DrawFeaturePolyline(locationTrack, PenTrackUAV_Id1)));
                                            break;
                                    }
                                }
                            }
                        }
                    }
                }), DispatcherPriority.Background);
            }
            catch { }
        }
        #endregion

        #region TrajectoryAeroscope 
        private List<ObjectDroneAero> trajectoryAeroscope;
        public List<ObjectDroneAero> TrajectoryAeroscope
        {
            get => trajectoryAeroscope;
            set
            {
                if (trajectoryAeroscope != null && trajectoryAeroscope.Equals(value)) return;
                trajectoryAeroscope = value;
                UpdateTrajectoryAeroscope();
            }
        }
        private void UpdateTrajectoryAeroscope()
        {
            try
            {
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    if (((MapViewModel)DataContext).TrajectoryAeroModel != null)
                    {
                        for (int i = 0; i < ((MapViewModel)DataContext).TrajectoryAeroModel.Count; i++)
                        {
                            mapControl.RemoveObject(((MapViewModel)DataContext).TrajectoryAeroModel[i].MapObject);
                            mapControl.RemoveObject(((MapViewModel)DataContext).TrajectoryAeroModel[i].MapTrack);
                        }
                    }

                    if (TrajectoryAeroscope.Count > 0)
                    {
                        ((MapViewModel)DataContext).TrajectoryAeroModel = new ObservableCollection<TrajectoryAeroscopeModel> { };

                        foreach (var trajectory in TrajectoryAeroscope)
                        {
                            if (trajectory.Trajectories.Count > 0)
                            {
                                int countPoints = 0;
                                double dist = 0;
                                for (int i = trajectory.Trajectories.Count - 1; i > 0; i--)
                                {
                                    double dist2Points = Bearing.ClassBearing.f_D_2Points(
                                        trajectory.Trajectories[i].Coordinates.Latitude,
                                        trajectory.Trajectories[i].Coordinates.Longitude,
                                        trajectory.Trajectories[i - 1].Coordinates.Latitude,
                                        trajectory.Trajectories[i - 1].Coordinates.Longitude, 1);

                                    dist += dist2Points;
                                    if (dist < LengthOfTrack)
                                    {
                                        countPoints += 1;
                                    }
                                    else break;
                                }

                                int count = trajectory.Trajectories.Count - countPoints;

                                IEnumerable<TableAeroscopeTrajectory> table = trajectory.Trajectories.Skip(count);
                                List<Location> locationTrack = new List<Location>(trajectory.Trajectories.Count - count);

                                foreach (var track in table)
                                    locationTrack.Add(new Location(track.Coordinates.Latitude, track.Coordinates.Longitude, track.Coordinates.Altitude));

                                ((MapViewModel)DataContext).TrajectoryAeroModel.Add(new TrajectoryAeroscopeModel(DrawObject(droneAeroscopeObjectStyle, trajectory.UAVRes.Type.ToString(),
                                    new Location(trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Longitude,
                                                 trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Latitude,
                                                 trajectory.Trajectories[trajectory.Trajectories.Count - 1].Coordinates.Altitude)),
                                                 DrawFeaturePolygon(TypeFeature.Polyline, locationTrack, BrushTrackAeroscope, PenTrackAeroscope, 2)));
                            }
                        }
                    }
                }), DispatcherPriority.Background);
            }
            catch { }
        }
        #endregion

        #region LinesUAVResTest
        private List<ObjectDrone> linesUAVResTest;
        public List<ObjectDrone> LinesUAVResTest
        {
            get => linesUAVResTest;
            set
            {
                linesUAVResTest = value;
                UpdateLinesUAVResTest();
            }
        }
        private void UpdateLinesUAVResTest()
        {
            try
            {
                Dispatcher.BeginInvoke(new Action(() =>
                {

                    if (((MapViewModel)DataContext).LinesModel != null)
                    {
                        for (int i = 0; i < ((MapViewModel)DataContext).LinesModel.Count; i++)
                        {
                            mapControl.RemoveObject(((MapViewModel)DataContext).LinesModel[i].MapObject);
                            mapControl.RemoveObject(((MapViewModel)DataContext).LinesModel[i].MapTrack);
                        }
                    }
                    if (tbShowLinesTest.IsChecked.Value)
                    {
                        if (LinesUAVResTest.Count > 0)
                        {
                            ((MapViewModel)DataContext).LinesModel = new ObservableCollection<TrajectoryModel> { };

                            foreach (var points in LinesUAVResTest)
                            {
                                if (points.Trajectories.Count > 0)
                                {
                                    List<Location> locationTrack = new List<Location>(points.Trajectories.Count);

                                    foreach (var track in points.Trajectories)
                                        locationTrack.Add(new Location(track.Coordinates.Latitude, track.Coordinates.Longitude, track.Coordinates.Altitude));

                                    ((MapViewModel)DataContext).LinesModel.Add(new TrajectoryModel(DrawObject(dronePointsObjectStyle, string.Empty,
                                                    new Location(points.Trajectories[points.Trajectories.Count - 1].Coordinates.Longitude,
                                                                 points.Trajectories[points.Trajectories.Count - 1].Coordinates.Latitude,
                                                                 points.Trajectories[points.Trajectories.Count - 1].Coordinates.Altitude)),
                                                    DrawFeaturePolygon(TypeFeature.Polyline, locationTrack, BrushTrackLines, PenTrackLines, 1)));
                                }
                            }
                        }
                    }
                }), DispatcherPriority.Background);
            }
            catch { }
        }
        #endregion

        #region PointsUAVResTest
        private List<ZoneParams> pointsUAVResTest;
        public List<ZoneParams> PointsUAVResTest
        {
            get => pointsUAVResTest;
            set
            {
                pointsUAVResTest = value;
                UpdatePointsUAVResTest();
            }
        }
        private void UpdatePointsUAVResTest()
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                if (((MapViewModel)DataContext).PointsModel != null)
                {
                    for (int i = 0; i < ((MapViewModel)DataContext).PointsModel.Count; i++)
                    {
                        mapControl.RemoveObject(((MapViewModel)DataContext).PointsModel[i].Zone);
                    }
                }

                if (tbShowPointsTest.IsChecked.Value)
                {
                    ((MapViewModel)DataContext).PointsModel = new ObservableCollection<ZoneModel> { };

                    if (PointsUAVResTest.Count > 0)
                    {
                        

                        for (int i = 0; i < PointsUAVResTest.Count; i++)
                        {
                            Location location = new Location(PointsUAVResTest[i].Latitude, PointsUAVResTest[i].Longitude);

                            ((MapViewModel)DataContext).PointsModel.Add(new ZoneModel(DrawFeaturePoint(location, PenTrackPoints)));
                        }

                        // -------------------------------------------------------------------------------------------------------------------------------
                        // Отображаются лучи
                        //for (int i = 0; i < PointsUAVResTest.Count; i++)
                        //{
                        //    Location location = new Location(PointsUAVResTest[i].Latitude, PointsUAVResTest[i].Longitude);
                           
                        //    ((MapViewModel)DataContext).PointsModel.Add(new ZoneModel(DrawFeatureRay(location, PenTrackPoints)));
                        //}
                        // -------------------------------------------------------------------------------------------------------------------------------

                        // -------------------------------------------------------------------------------------------------------------------------------
                        // Отображаются кружки (тормозит!)
                        //for (int i = 0; i < PointsUAVResTest.Count; i++)
                        //{
                        //    List<Location> ListCoord = new List<Location>(360);

                        //    for (int j = 0; j < 360; j++)
                        //    {
                        //        ListCoord.Add(DefinePointSphere(new Location(PointsUAVResTest[i].Latitude, PointsUAVResTest[i].Longitude), j, PointsUAVResTest[i].Radius));
                        //    }

                        //   ((MapViewModel)DataContext).PointsModel.Add(new ZoneModel(DrawFeaturePolygon(TypeFeature.Polygon, ListCoord, BrushTrackPoints, PenTrackPoints, 1)));
                        //}
                        // -------------------------------------------------------------------------------------------------------------------------------
                    }
                }
            }), DispatcherPriority.Background);
        }
        #endregion

        #region EllipseTest
        // последняя точка траектории для строба
        private List<TableTrajectory> lastPointTrack { get; set; } = new List<TableTrajectory>();
        public List<TableTrajectory> LastPointTrack
        {
            get => lastPointTrack;
            set
            {
                lastPointTrack = value;
                UpdateLastPointTrack();
            }
        }

        private void UpdateLastPointTrack()
        {
            if (!mapControl.IsMapLoaded)
            {
                return;
            }

            listEllipseParams = new List<EllipseParams>();

            for (int i = 0; i < LastPointTrack.Count; i++)
            {
                listEllipseParams.Add(new EllipseParams
                {
                    Head = LastPointTrack[i].Head,
                    RadiusA = (short)LastPointTrack[i].RadiusA,
                    RadiusB = (short)LastPointTrack[i].RadiusB,
                    Latitude = LastPointTrack[i].Coordinates.Longitude,
                    Longitude = LastPointTrack[i].Coordinates.Latitude
                });
            }

            EllipseTest = listEllipseParams;
        }

        private List<EllipseParams> ellipseTest;
        public List<EllipseParams> EllipseTest
        {
            get => ellipseTest;
            set
            {
                ellipseTest = value;
                UpdateEllipseTest();
            }
        }
        private void UpdateEllipseTest()
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                if (((MapViewModel)DataContext).EllipseModel != null)
                {
                    for (int i = 0; i < ((MapViewModel)DataContext).EllipseModel.Count; i++)
                    {
                        mapControl.RemoveObject(((MapViewModel)DataContext).EllipseModel[i].Zone);
                    }
                }

                // Строб
                if (tbShowEllipseTest.IsChecked.Value)
                {
                    for (int i = 0; i < EllipseTest.Count; i++)
                    {

                        List<Location> ListCoord = new List<Location>(360);
                        for (int j = 0; j < 360; j++)
                        {
                            // Радиус эллипса в каждом градусе -----------------------------------
                            double radius = GetRadiusEllipse(new Location(EllipseTest[i].Latitude, EllipseTest[i].Longitude),
                                EllipseTest[i].RadiusA, EllipseTest[i].RadiusB, j);
                            //--------------------------------------------------------------------

                            ListCoord.Add(DefinePointEllipse(new Location(EllipseTest[i].Latitude, EllipseTest[i].Longitude), j, radius, EllipseTest[i].Head));
                        }

                        ((MapViewModel)DataContext).EllipseModel.Add(new ZoneModel(DrawFeaturePolygon(TypeFeature.Polygon, ListCoord, BrushEllipse, PenEllipse, 1)));
                    }
                }
            }), DispatcherPriority.Background);
        }
        #endregion

        #region Route
        private Location startPointRoute = new Location(-1, -1);
        public Location StartPointRoute
        {
            get => startPointRoute;
            set
            {
                startPointRoute = value;
                UpdateStartPosition();
                DrawRoute();
                OnPropertyChanged();
            }
        }

        private Location stopPointRoute = new Location(-1, -1);
        public Location StopPointRoute
        {
            get => stopPointRoute;
            set
            {
                stopPointRoute = value;
                UpdateStopPosition(0);
                DrawRoute();
                OnPropertyChanged();
            }
        }

        private void UpdateStartPosition()
        {
            try
            {
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    if (((MapViewModel)DataContext).StartPointRouteModel != null)
                    {
                        for (int i = 0; i < ((MapViewModel)DataContext).StartPointRouteModel.Count; i++)
                        {
                            mapControl.RemoveObject(((MapViewModel)DataContext).StartPointRouteModel[i].MapObject);
                        }
                    }
                    ((MapViewModel)DataContext).StartPointRouteModel = new ObservableCollection<PointRouteModel> { };

                    if (StartPointRoute.Latitude != -1 && StartPointRoute.Longitude != -1)
                        ((MapViewModel)DataContext).StartPointRouteModel.Add(new PointRouteModel(DrawObject(startPointDistanceObjectStyle, StartPointRoute)));
                       
                }), DispatcherPriority.Background);
            }
            catch
            { }
        }

        private void UpdateStopPosition(float Distance)
        {
            try
            {
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    if (((MapViewModel)DataContext).StopPointRouteModel != null)
                    {
                        for (int i = 0; i < ((MapViewModel)DataContext).StopPointRouteModel.Count; i++)
                        {
                            mapControl.RemoveObject(((MapViewModel)DataContext).StopPointRouteModel[i].MapObject);
                        }
                    }
                    ((MapViewModel)DataContext).StopPointRouteModel = new ObservableCollection<PointRouteModel> { };

                    if (StopPointRoute.Latitude != -1 && StopPointRoute.Longitude != -1)
                        ((MapViewModel)DataContext).StopPointRouteModel.Add(new PointRouteModel(DrawObject(stopPointDistanceObjectStyle, Distance.ToString() + " m", StopPointRoute)));

                }), DispatcherPriority.Background);
            }
            catch
            { }
        }

        private void DrawRoute()
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                if (((MapViewModel)DataContext).RouteModel != null)
                {
                    for (int i = 0; i < ((MapViewModel)DataContext).RouteModel.Count; i++)
                    {
                        mapControl.RemoveObject(((MapViewModel)DataContext).RouteModel[i].FeatureRoute);
                    }
                }
                ((MapViewModel)DataContext).RouteModel = new ObservableCollection<RouteModel> { };


            }), DispatcherPriority.Background);

            if (StartPointRoute.Latitude != -1 && StartPointRoute.Longitude != -1 &&
                StopPointRoute.Latitude != -1 && StopPointRoute.Longitude != -1)
            {
                try
                {
                    List<Location> locationDistance = new List<Location>();
                    locationDistance.Add(new Location(StartPointRoute.Latitude, StartPointRoute.Longitude));
                    locationDistance.Add(new Location(StopPointRoute.Latitude, StopPointRoute.Longitude));

                    var dist = CalculateDistance(StartPointRoute, StopPointRoute);

                    UpdateStopPosition(dist);

                    Dispatcher.BeginInvoke(new Action(() =>
                    {
                        ((MapViewModel)DataContext).RouteModel.Add(new RouteModel(DrawFeaturePolygon(TypeFeature.Polyline, locationDistance, BrushDistance, PenDistance, 0.5)));

                    }), DispatcherPriority.Background);
                }
                catch { }
            }
        }
        #endregion

        #region Location Operator Cuirasse-M
        private Location locationOperator = new Location(-1, -1);
        public Location LocationOperator
        {
            get => locationOperator;
            set
            {
                locationOperator = value;
                UpdateLocationOperator();
            }
        }

        private void UpdateLocationOperator()
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                if (((MapViewModel)DataContext).LocationOperatorModel != null)
                {
                    for (int i = 0; i < ((MapViewModel)DataContext).LocationOperatorModel.Count; i++)
                    {
                        mapControl.RemoveObject(((MapViewModel)DataContext).LocationOperatorModel[i].MapObject);
                    }
                }
                    ((MapViewModel)DataContext).LocationOperatorModel = new ObservableCollection<LocationImageModel> { };

                if (LocationOperator.Latitude != -1 && LocationOperator.Longitude != -1)
                {
                    ((MapViewModel)DataContext).LocationOperatorModel.Add(new LocationImageModel(DrawObject(operatorObjectStyle, "",/*Operator*/
                        new Location(LocationOperator.Longitude, LocationOperator.Latitude, LocationOperator.Altitude))));

                }

            }), DispatcherPriority.Background);
        }
        #endregion

        #region Location Groza-S
        private Location locationGrozaS = new Location(-1, -1);
        public Location LocationGrozaS
        {
            get => locationGrozaS;
            set
            {
                locationGrozaS = value;
                UpdateLocationGrozaS(); 
            }
        }
        private void UpdateLocationGrozaS()
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                if (((MapViewModel)DataContext).LocationGrozaSModel != null)
                {
                    for (int i = 0; i < ((MapViewModel)DataContext).LocationGrozaSModel.Count; i++)
                    {
                        mapControl.RemoveObject(((MapViewModel)DataContext).LocationGrozaSModel[i].MapObject);
                    }
                }
                    ((MapViewModel)DataContext).LocationGrozaSModel = new ObservableCollection<LocationImageModel> { };

                if (LocationGrozaS.Latitude != -1 && LocationGrozaS.Longitude != -1)
                    ((MapViewModel)DataContext).LocationGrozaSModel.Add(new LocationImageModel(DrawObject(grozaSObjectStyle, "", LocationGrozaS)));/*Groza-S*/

            }), DispatcherPriority.Background);
        }

        #endregion
    }
}
