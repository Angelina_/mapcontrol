﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace MapControl
{
    public class MapViewModel : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        #endregion

        public ObservableCollection<ReceivingPointModel> LocalReceivingPointModel { get; set; }
        public ObservableCollection<ReceivingPointModel> RemoteReceivingPointModel { get; set; }
        public ObservableCollection<ZoneModel> ZoneModel { get; set; }
        public LocationModel LocationModel { get; set; }

        public ObservableCollection<TrajectoryModel> TrajectoryModel { get; set; }
        public ObservableCollection<TrajectoryModel> TrajectoryArchiveModel { get; set; }
        public ObservableCollection<TrajectoryAeroscopeModel> TrajectoryAeroModel { get; set; }
        public ObservableCollection<TrajectoryModel> LinesModel { get; set; }
        public ObservableCollection<ZoneModel> PointsModel { get; set; }
        public ObservableCollection<ZoneModel> EllipseModel { get; set; }
        public ObservableCollection<PointRouteModel> StartPointRouteModel { get; set; }
        public ObservableCollection<PointRouteModel> StopPointRouteModel { get; set; }
        public ObservableCollection<RouteModel> RouteModel { get; set; }
        public ObservableCollection<LocationImageModel> LocationOperatorModel { get; set; }
        public ObservableCollection<LocationImageModel> LocationGrozaSModel { get; set; }

        public MapViewModel()
        {
            LocalReceivingPointModel = new ObservableCollection<ReceivingPointModel> { };
            RemoteReceivingPointModel = new ObservableCollection<ReceivingPointModel> { };
            ZoneModel = new ObservableCollection<ZoneModel> { };
            LocationModel = new LocationModel { };

            TrajectoryModel = new ObservableCollection<TrajectoryModel> { };
            TrajectoryArchiveModel = new ObservableCollection<TrajectoryModel> { };
            TrajectoryAeroModel = new ObservableCollection<TrajectoryAeroscopeModel> { };
            LinesModel = new ObservableCollection<TrajectoryModel> { };
            PointsModel = new ObservableCollection<ZoneModel> { };
            EllipseModel = new ObservableCollection<ZoneModel> { };

            StartPointRouteModel = new ObservableCollection<PointRouteModel> { }; 
            StopPointRouteModel = new ObservableCollection<PointRouteModel> { };
            RouteModel = new ObservableCollection<RouteModel> { };

            LocationOperatorModel = new ObservableCollection<LocationImageModel> { };
            LocationGrozaSModel = new ObservableCollection<LocationImageModel> { };
        }
    }
}
