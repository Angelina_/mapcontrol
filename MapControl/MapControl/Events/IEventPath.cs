﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapControl
{
    public interface IEventPath
    {
        event EventHandler<EventPath> OnChangeFileMap;
    }
}
