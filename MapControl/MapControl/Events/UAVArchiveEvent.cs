﻿using KirasaModelsDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapControl
{
    public class UAVArchiveEvent : EventArgs
    {
        public TableUAVBase TableUAVBase { get; set; }

        public List<TableTrajectory> ListTracks { get; set; }

        public UAVArchiveEvent(TableUAVBase table, List<TableTrajectory> list)
        {
            TableUAVBase = table;
            ListTracks = list;
        }
    }
}
