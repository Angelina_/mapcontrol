﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapControl
{
    public class EventPath : EventArgs
    {
        public string FileMap { get; }

        public EventPath(string fileMap)
        {
            FileMap = fileMap;
        }
    }
}
