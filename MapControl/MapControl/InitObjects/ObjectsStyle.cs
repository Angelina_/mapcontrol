﻿using Mapsui.Styles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using WpfMapControl;

namespace MapControl
{
    public partial class UserControlMap : UserControl
    {
        # region MapObjectStyle
        private MapObjectStyle receivingLocalPointOwnObjectStyle;
        private MapObjectStyle receivingLocalPointObjectStyle;

        private MapObjectStyle receivingRemotePointOwnObjectStyle;
        private MapObjectStyle receivingRemotePointObjectStyle;

        private MapObjectStyle startPointDistanceObjectStyle;
        private MapObjectStyle stopPointDistanceObjectStyle;

        private MapObjectStyle droneAeroscopeObjectStyle;

        private MapObjectStyle dronePointsObjectStyle;

        private MapObjectStyle droneUAVResId1ObjectStyle;
        private MapObjectStyle droneUAVResId2ObjectStyle;
        private MapObjectStyle droneUAVResId3ObjectStyle;
        private MapObjectStyle droneUAVResId4ObjectStyle;
        private MapObjectStyle droneUAVResId5ObjectStyle;
        private MapObjectStyle droneUAVResId6ObjectStyle;
        private MapObjectStyle droneUAVResId7ObjectStyle;
        private MapObjectStyle droneUAVResId8ObjectStyle;
        private MapObjectStyle droneUAVResId9ObjectStyle;
        private MapObjectStyle droneUAVResId10ObjectStyle;

        private MapObjectStyle droneUAVResArchiveId1ObjectStyle;
        private MapObjectStyle droneUAVResArchiveId2ObjectStyle;
        private MapObjectStyle droneUAVResArchiveId3ObjectStyle;
        private MapObjectStyle droneUAVResArchiveId4ObjectStyle;
        private MapObjectStyle droneUAVResArchiveId5ObjectStyle;
        private MapObjectStyle droneUAVResArchiveId6ObjectStyle;
        private MapObjectStyle droneUAVResArchiveId7ObjectStyle;
        private MapObjectStyle droneUAVResArchiveId8ObjectStyle;
        private MapObjectStyle droneUAVResArchiveId9ObjectStyle;
        private MapObjectStyle droneUAVResArchiveId10ObjectStyle;

        private MapObjectStyle grozaSObjectStyle;
        private MapObjectStyle operatorObjectStyle;


        #endregion
        public void InitObjectsStyle()
        {
            #region Локальный пункт
            try
            {
                // Локальный пункт (свой)
                receivingLocalPointOwnObjectStyle = mapControl.LoadObjectStyle(ImageLocalPoint,
                    scale: 0.3);

                //receivingPointOwnObjectStyle = mapControl.LoadObjectStyle(AppDomain.CurrentDomain.BaseDirectory + "/Images/ReceivingPoint.png",
                //   scale: 0.3);

            }
            catch { }

            try
            {
                // Локальный пункт 
                receivingLocalPointObjectStyle = mapControl.LoadObjectStyle(ImageLocalPoint,
                    scale: 0.2,
                    objectOffset: new Offset(0, 0),
                    textOffset: new Offset(0, 6));

                //receivingPointObjectStyle = mapControl.LoadObjectStyle(AppDomain.CurrentDomain.BaseDirectory + "/Images/ReceivingPoint.png",
                //    scale: 0.2,
                //    objectOffset: new Offset(0, 0),
                //    textOffset: new Offset(0, 6));
            }
            catch { }
            #endregion

            #region Удаленный пункт
            try
            {
                // Удаленный пункт (свой)
                receivingRemotePointOwnObjectStyle = mapControl.LoadObjectStyle(ImageRemotePoint,
                    scale: 0.3);
            }
            catch { }

            try
            {
                // Удаленный пункт 
                receivingRemotePointObjectStyle = mapControl.LoadObjectStyle(ImageRemotePoint,
                    scale: 0.2,
                    objectOffset: new Offset(0, 0),
                    textOffset: new Offset(0, 6));

            }
            catch { }
            #endregion

            #region Дрон Аэроскоп
            try
            {
                // Дрон Аэроскоп
                droneAeroscopeObjectStyle = mapControl.LoadObjectStyle(FolderImageSource + "/DroneAeroscope.png",
                  scale: 0.1,
                  objectOffset: new Offset(0, 0),
                  textOffset: new Offset(0, 6));
                //droneAeroscopeObjectStyle = mapControl.LoadObjectStyle(AppDomain.CurrentDomain.BaseDirectory + "/Images/DroneAeroscope.png",
                //       scale: 0.1,
                //       objectOffset: new Offset(0, 0),
                //       textOffset: new Offset(0, 6));
            }
            catch { }
            #endregion

            #region Дрон Points
            try
            {
                // Дрон Points
                dronePointsObjectStyle = mapControl.LoadObjectStyle(FolderImageSource + "/DronePoints.png",
                  scale: 0.1,
                  objectOffset: new Offset(0, 0),
                  textOffset: new Offset(0, 6));
            }
            catch { }
            #endregion

            #region Дроны ИРИ БПЛА 
            try
            {
                // Дрон ИРИ БПЛА Id1
                droneUAVResId1ObjectStyle = mapControl.LoadObjectStyle(FolderImageSource + "/DroneUAV_Id1.png",
                   scale: 0.1,
                   objectOffset: new Offset(0, 0),
                   textOffset: new Offset(0, 6));
            }
            catch { }

            try
            {
                // Дрон ИРИ БПЛА Id2
                droneUAVResId2ObjectStyle = mapControl.LoadObjectStyle(FolderImageSource + "/DroneUAV_Id2.png",
                  scale: 0.1,
                  objectOffset: new Offset(0, 0),
                  textOffset: new Offset(0, 6));
            }
            catch { }

            try
            {
                // Дрон ИРИ БПЛА Id3
                droneUAVResId3ObjectStyle = mapControl.LoadObjectStyle(FolderImageSource + "/DroneUAV_Id3.png",
                  scale: 0.1,
                  objectOffset: new Offset(0, 0),
                  textOffset: new Offset(0, 6));
            }
            catch { }

            try
            {
                // Дрон ИРИ БПЛА Id4
                droneUAVResId4ObjectStyle = mapControl.LoadObjectStyle(FolderImageSource + "/DroneUAV_Id4.png",
                 scale: 0.1,
                 objectOffset: new Offset(0, 0),
                 textOffset: new Offset(0, 6));
            }
            catch { }

            try
            {
                // Дрон ИРИ БПЛА Id5
                droneUAVResId5ObjectStyle = mapControl.LoadObjectStyle(FolderImageSource + "/DroneUAV_Id5.png",
                scale: 0.1,
                objectOffset: new Offset(0, 0),
                textOffset: new Offset(0, 6));
            }
            catch { }

            try
            {
                // Дрон ИРИ БПЛА Id6
                droneUAVResId6ObjectStyle = mapControl.LoadObjectStyle(FolderImageSource + "/DroneUAV_Id6.png",
                scale: 0.1,
                objectOffset: new Offset(0, 0),
                textOffset: new Offset(0, 6));
            }
            catch { }

            try
            {
                // Дрон ИРИ БПЛА Id7
                droneUAVResId7ObjectStyle = mapControl.LoadObjectStyle(FolderImageSource + "/DroneUAV_Id7.png",
                scale: 0.1,
                objectOffset: new Offset(0, 0),
                textOffset: new Offset(0, 6));
            }
            catch { }

            try
            {
                // Дрон ИРИ БПЛА Id8
                droneUAVResId8ObjectStyle = mapControl.LoadObjectStyle(FolderImageSource + "/DroneUAV_Id8.png",
                scale: 0.1,
                objectOffset: new Offset(0, 0),
                textOffset: new Offset(0, 6));
            }
            catch { }

            try
            {
                // Дрон ИРИ БПЛА Id9
                droneUAVResId9ObjectStyle = mapControl.LoadObjectStyle(FolderImageSource + "/DroneUAV_Id9.png",
                scale: 0.1,
                objectOffset: new Offset(0, 0),
                textOffset: new Offset(0, 6));
            }
            catch { }

            try
            {
                // Дрон ИРИ БПЛА Id10
                droneUAVResId10ObjectStyle = mapControl.LoadObjectStyle(FolderImageSource + "/DroneUAV_Id10.png",
                scale: 0.1,
                objectOffset: new Offset(0, 0),
                textOffset: new Offset(0, 6));
            }
            catch { }
            #endregion

            #region Дроны ИРИ БПЛА архив
            try
            {
                // Дрон ИРИ БПЛА архив Id1
                droneUAVResArchiveId1ObjectStyle = mapControl.LoadObjectStyle(FolderImageSource + "/DroneUAVArchive_Id1.png",
                   scale: 0.1,
                   objectOffset: new Offset(0, 0),
                   textOffset: new Offset(0, 6));
            }
            catch { }

            try
            {
                // Дрон ИРИ БПЛА архив Id2
                droneUAVResArchiveId2ObjectStyle = mapControl.LoadObjectStyle(FolderImageSource + "/DroneUAVArchive_Id2.png",
                  scale: 0.1,
                  objectOffset: new Offset(0, 0),
                  textOffset: new Offset(0, 6));
            }
            catch { }

            try
            {
                // Дрон ИРИ БПЛА архив Id3
                droneUAVResArchiveId3ObjectStyle = mapControl.LoadObjectStyle(FolderImageSource + "/DroneUAVArchive_Id3.png",
                  scale: 0.1,
                  objectOffset: new Offset(0, 0),
                  textOffset: new Offset(0, 6));
            }
            catch { }

            try
            {
                // Дрон ИРИ БПЛА архив Id4
                droneUAVResArchiveId4ObjectStyle = mapControl.LoadObjectStyle(FolderImageSource + "/DroneUAVArchive_Id4.png",
                 scale: 0.1,
                 objectOffset: new Offset(0, 0),
                 textOffset: new Offset(0, 6));
            }
            catch { }

            try
            {
                // Дрон ИРИ БПЛА архив Id5
                droneUAVResArchiveId5ObjectStyle = mapControl.LoadObjectStyle(FolderImageSource + "/DroneUAVArchive_Id5.png",
                scale: 0.1,
                objectOffset: new Offset(0, 0),
                textOffset: new Offset(0, 6));
            }
            catch { }

            try
            {
                // Дрон ИРИ БПЛА архив Id6
                droneUAVResArchiveId6ObjectStyle = mapControl.LoadObjectStyle(FolderImageSource + "/DroneUAVArchive_Id6.png",
                scale: 0.1,
                objectOffset: new Offset(0, 0),
                textOffset: new Offset(0, 6));
            }
            catch { }

            try
            {
                // Дрон ИРИ БПЛА архив Id7
                droneUAVResArchiveId7ObjectStyle = mapControl.LoadObjectStyle(FolderImageSource + "/DroneUAVArchive_Id7.png",
                scale: 0.1,
                objectOffset: new Offset(0, 0),
                textOffset: new Offset(0, 6));
            }
            catch { }

            try
            {
                // Дрон ИРИ БПЛА архив Id8
                droneUAVResArchiveId8ObjectStyle = mapControl.LoadObjectStyle(FolderImageSource + "/DroneUAVArchive_Id8.png",
                scale: 0.1,
                objectOffset: new Offset(0, 0),
                textOffset: new Offset(0, 6));
            }
            catch { }

            try
            {
                // Дрон ИРИ БПЛА архив Id9
                droneUAVResArchiveId9ObjectStyle = mapControl.LoadObjectStyle(FolderImageSource + "/DroneUAVArchive_Id9.png",
                scale: 0.1,
                objectOffset: new Offset(0, 0),
                textOffset: new Offset(0, 6));
            }
            catch { }

            try
            {
                // Дрон ИРИ БПЛА архив Id10
                droneUAVResArchiveId10ObjectStyle = mapControl.LoadObjectStyle(FolderImageSource + "/DroneUAVArchive_Id10.png",
                scale: 0.1,
                objectOffset: new Offset(0, 0),
                textOffset: new Offset(0, 6));
            }
            catch { }
            #endregion

            #region Начало/конец расстояния
            try
            {
                //var rm = new System.Resources.ResourceManager(((System.Reflection.Assembly)System.Reflection.Assembly.GetExecutingAssembly()).GetName().Name + ".Properties.Resources", ((System.Reflection.Assembly)System.Reflection.Assembly.GetExecutingAssembly()));

                //BitmapImage StartPointDistance = (BitmapImage)rm.GetObject("StartPointDistance");

                // Начало
                startPointDistanceObjectStyle = mapControl.LoadObjectStyle(AppDomain.CurrentDomain.BaseDirectory + "/Images/StartPointDistance.png",
                    scale: 0.5);
            }
            catch { }

            try
            {
                // Конец
                stopPointDistanceObjectStyle = mapControl.LoadObjectStyle(AppDomain.CurrentDomain.BaseDirectory + "/Images/StopPointDistance.png",
                    scale: 0.5,
                    objectOffset: new Offset(0, 0),
                    textOffset: new Offset(0, 6));
            }
            catch { }
            #endregion

            #region Гроза-С
            try
            {
                // Гроза-С
                grozaSObjectStyle = mapControl.LoadObjectStyle(AppDomain.CurrentDomain.BaseDirectory + "/Images/LocationGrozaS.png",
                  scale: 0.65,
                  objectOffset: new Offset(0, 0),
                  textOffset: new Offset(0, 16));
            }
            catch { }
            #endregion

            #region Оператор Кираса-М
            try
            {
                // Оператор Кираса-М
                operatorObjectStyle = mapControl.LoadObjectStyle(AppDomain.CurrentDomain.BaseDirectory + "/Images/LocationOperator.png", //"D:\\WorkWPF\\CuirasseM\\RepCuirasseM\\Cuirasse\\bin\\Debug\\Images\\LocationOperator.png"
                  scale: 0.35,
                  objectOffset: new Offset(0, 0),
                  textOffset: new Offset(0, 8));
            }
            catch { }
            #endregion

        }
    }
}
