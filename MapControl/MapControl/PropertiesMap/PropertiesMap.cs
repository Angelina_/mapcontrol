﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using WpfMapControl;

namespace MapControl
{
    public partial class UserControlMap : UserControl, INotifyPropertyChanged
    {
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        #endregion

        private bool isShowZones;
        public bool IsShowZones
        {
            get { return isShowZones; }
            set
            {
                if (isShowZones != value)
                {
                    isShowZones = value;
                    OnPropertyChanged();
                    //TBChecked(isShowZones);
                }
            }
        }
        public string FileMap { get; set; }
        public string FolderImageSource { get; set; }
        public string ImageLocalPoint { get; set; }
        public string ImageRemotePoint { get; set; }
        public byte Projection { get; set; }
        public short LengthOfTrack { get; set; }

        private string folderMapTiles;
        public string FolderMapTiles
        {
            get => folderMapTiles;
            set
            {
                if (folderMapTiles != null && folderMapTiles.Equals(value)) return;
                folderMapTiles = value;
                UpdateFolderMapTiles();
            }
        }

        private void UpdateFolderMapTiles()
        {
            mapControl.SetDataPath(FolderMapTiles);
        }

        private byte isAdmin;
        public byte IsAdmin
        {
            get { return isAdmin; }
            set
            {
                if (isAdmin != value)
                {
                    isAdmin = value;
                    OnPropertyChanged();
                    ShowElementsTest(isAdmin);
                }
            }
        }

        private short amountOfDefinePoint;
        public short AmountOfDefinePoint
        { 
            get { return amountOfDefinePoint; }
            set
            {
                if (amountOfDefinePoint != value)
                {
                    amountOfDefinePoint = value;
                    listLinesUAVRes = new List<KirasaModelsDBLib.TableTrajectory>();
                    listPointsUAVRes = new List<ZoneParams>();
                    OnPropertyChanged();
                }
            }
        }
      



        //public bool IsShowZones
        //{
        //    get 
        //    { 
        //        return (bool)this.GetValue(IsShowZonesProperty); 
        //    }
        //    set 
        //    { 
        //        this.SetValue(IsShowZonesProperty, value); 
        //    }
        //}
        //public static readonly DependencyProperty IsShowZonesProperty = DependencyProperty.Register(
        //  "IsShowZones", typeof(bool), typeof(UserControlMap), new PropertyMetadata(false));
    }

   
}
