﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace MapControl
{
    public partial class UserControlMap : UserControl
    {
        /// <summary>
        /// Для ИРИ БПЛА
        /// </summary>
        // Красный
        Mapsui.Styles.Pen PenTrackUAV_Id1 = new Mapsui.Styles.Pen(Mapsui.Styles.Color.FromArgb(255, 254, 0, 0), 1);
        Mapsui.Styles.Color BrushTrackUAV_Id1 = Mapsui.Styles.Color.FromArgb(200, 254, 0, 0);
        // Синий
        Mapsui.Styles.Pen PenTrackUAV_Id2 = new Mapsui.Styles.Pen(Mapsui.Styles.Color.FromArgb(255, 0, 0, 254), 1);
        Mapsui.Styles.Color BrushTrackUAV_Id2 = Mapsui.Styles.Color.FromArgb(200, 0, 0, 254);
        // Салатовый
        Mapsui.Styles.Pen PenTrackUAV_Id3 = new Mapsui.Styles.Pen(Mapsui.Styles.Color.FromArgb(255, 0, 210, 1), 1);
        Mapsui.Styles.Color BrushTrackUAV_Id3 = Mapsui.Styles.Color.FromArgb(200, 0, 210, 1);
        // Оранжевый
        Mapsui.Styles.Pen PenTrackUAV_Id4 = new Mapsui.Styles.Pen(Mapsui.Styles.Color.FromArgb(255, 255, 115, 0), 1);
        Mapsui.Styles.Color BrushTrackUAV_Id4 = Mapsui.Styles.Color.FromArgb(200, 255, 115, 0);
        // Фиолетовый
        Mapsui.Styles.Pen PenTrackUAV_Id5 = new Mapsui.Styles.Pen(Mapsui.Styles.Color.FromArgb(255, 101, 0, 128), 1);
        Mapsui.Styles.Color BrushTrackUAV_Id5 = Mapsui.Styles.Color.FromArgb(200, 101, 0, 128);
        // Сиреневый
        Mapsui.Styles.Pen PenTrackUAV_Id6 = new Mapsui.Styles.Pen(Mapsui.Styles.Color.FromArgb(255, 111, 112, 254), 1);
        Mapsui.Styles.Color BrushTrackUAV_Id6 = Mapsui.Styles.Color.FromArgb(200, 111, 112, 254);
        // Зеленый
        Mapsui.Styles.Pen PenTrackUAV_Id7 = new Mapsui.Styles.Pen(Mapsui.Styles.Color.FromArgb(255, 0, 96, 0), 1);
        Mapsui.Styles.Color BrushTrackUAV_Id7 = Mapsui.Styles.Color.FromArgb(200, 0, 96, 0);
        // Розовый
        Mapsui.Styles.Pen PenTrackUAV_Id8 = new Mapsui.Styles.Pen(Mapsui.Styles.Color.FromArgb(255, 254, 124, 124), 1);
        Mapsui.Styles.Color BrushTrackUAV_Id8 = Mapsui.Styles.Color.FromArgb(200, 254, 124, 124);
        // Коричневый
        Mapsui.Styles.Pen PenTrackUAV_Id9 = new Mapsui.Styles.Pen(Mapsui.Styles.Color.FromArgb(255, 145, 66, 0), 1);
        Mapsui.Styles.Color BrushTrackUAV_Id9 = Mapsui.Styles.Color.FromArgb(200, 145, 66, 0);
        // Бледно-фиолетовый
        Mapsui.Styles.Pen PenTrackUAV_Id10 = new Mapsui.Styles.Pen(Mapsui.Styles.Color.FromArgb(255, 165, 112, 180), 1);
        Mapsui.Styles.Color BrushTrackUAV_Id10 = Mapsui.Styles.Color.FromArgb(200, 165, 112, 180);

        /// <summary>
        /// Для аэроскопа
        /// </summary>
        // Серый
        Mapsui.Styles.Pen PenTrackAeroscope = new Mapsui.Styles.Pen(Mapsui.Styles.Color.Gray, 2);
        Mapsui.Styles.Color BrushTrackAeroscope = Mapsui.Styles.Color.FromArgb(255, 255, 0, 255);

        /// <summary>
        /// Для Lines Test
        /// </summary>
        // Черный
        Mapsui.Styles.Pen PenTrackLines = new Mapsui.Styles.Pen(Mapsui.Styles.Color.Black, 1);
        Mapsui.Styles.Color BrushTrackLines = Mapsui.Styles.Color.FromArgb(200, 0, 0, 0);

        /// <summary>
        /// Для Points Test
        /// </summary>
        // Черный
        Mapsui.Styles.Pen PenTrackPoints = new Mapsui.Styles.Pen(Mapsui.Styles.Color.FromArgb(255, 0, 0, 0), 1);
        Mapsui.Styles.Color BrushTrackPoints = Mapsui.Styles.Color.FromArgb(20, 0, 0, 0);

        /// <summary>
        /// Для Ellipse
        /// </summary>
        // Синий
        Mapsui.Styles.Pen PenEllipse = new Mapsui.Styles.Pen(Mapsui.Styles.Color.FromArgb(255, 0, 0, 0), 1);
        Mapsui.Styles.Color BrushEllipse = Mapsui.Styles.Color.FromArgb(10, 0, 0, 0);

        /// <summary>
        /// Для Distance
        /// </summary>
        // Черный
        Mapsui.Styles.Pen PenDistance = new Mapsui.Styles.Pen(Mapsui.Styles.Color.Black, 1);
        Mapsui.Styles.Color BrushDistance = Mapsui.Styles.Color.FromArgb(200, 0, 0, 0);

        /// <summary>
        /// Для зон
        /// </summary>
        // Красный
        Mapsui.Styles.Pen PenZoneAlarm = new Mapsui.Styles.Pen(Mapsui.Styles.Color.FromArgb(255, 254, 0, 0), 1);
        Mapsui.Styles.Color BrushZoneAlarm = Mapsui.Styles.Color.FromArgb(20, 254, 0, 0);
        // Синий
        Mapsui.Styles.Pen PenZoneAttention = new Mapsui.Styles.Pen(Mapsui.Styles.Color.FromArgb(255, 0, 0, 254), 1);
        Mapsui.Styles.Color BrushZoneAttention = Mapsui.Styles.Color.FromArgb(20, 0, 0, 254);
    }
}
