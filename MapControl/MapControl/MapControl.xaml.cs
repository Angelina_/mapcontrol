﻿using KirasaModelsDBLib;
using Mapsui.Projection;
using Mapsui.Styles;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using WpfMapControl;

namespace MapControl
{
    /// <summary>
    /// Логика взаимодействия для UserControl1.xaml
    /// </summary>
    public partial class UserControlMap : UserControl
    {
        //System.Timers.Timer timer = new System.Timers.Timer();

        //MapViewModel mapViewModel = new MapViewModel();

        #region Events
        public event EventHandler<Location> OnUpdateZonesCenter;
        public event EventHandler<Location> OnSetLocalReceptionPoint;
        public event EventHandler<Location> OnSetRemoteReceptionPoint;
        public event EventHandler<bool> OnShowZones;
        public event EventHandler OnLocationGrozaS;
        #endregion


        public UserControlMap()
        {
            InitializeComponent();

          

            DataContext = new MapViewModel();

            mapControl.MapMouseMoveEvent += OnMapMouseMove;
            mapControl.ResolutionChangedEvent += OnResolutionCahnged;
            mapControl.MapClickEvent += OnMapClick;

            InitScaleMap();

            //InitObjectsStyle();

            //timer.Interval = 1000;
            //timer.Start();
            //timer.Elapsed += Timer_Elapsed;

        }


        private void OnMapClick(object sender, MapMouseClickEventArgs e)
        {
            if (e.ClickedButton.ChangedButton == MouseButton.Right)
            {
                switch ((MapProjection)Projection)
                {
                    case MapProjection.Geo:
                        ((MapViewModel)DataContext).LocationModel.ClickCoord = e.Location;
                        break;

                    case MapProjection.Mercator:
                        var p = Mapsui.Projection.Mercator.ToLonLat(e.Location.Longitude, e.Location.Latitude);
                        ((MapViewModel)DataContext).LocationModel.ClickCoord = new Location(p.X, p.Y, null);
                        break;

                    default:
                        break;
                }
            }
        }

        private void OnMapMouseMove(object sender, Location location)
        {
            switch ((MapProjection)Projection)
            {
                case MapProjection.Geo:
                    ((MapViewModel)DataContext).LocationModel.Coordinate = location;

                    break;

                case MapProjection.Mercator:
                    var p = Mapsui.Projection.Mercator.ToLonLat(location.Longitude, location.Latitude);
                    ((MapViewModel)DataContext).LocationModel.Coordinate = new Location(p.X, p.Y, null);
                    
                    break;

                default:
                    break;
            }
        }

        private void OnResolutionCahnged(object sender, double resolution)
        {
            try
            {
                SliderZoom.Value = SliderZoom.Maximum - resolution + SliderZoom.Minimum;
            }
            catch { }
        }

        private void ButtonMapOpen_Click(object sender, RoutedEventArgs e)
        {
            OpenDialogMap();

            SetScaleParams();
        }

        private void ButtonMapClose_Click(object sender, RoutedEventArgs e)
        {
            CloseMap();
        }

        private void ButtonMapCenter_Click(object sender, RoutedEventArgs e)
        {
            CenteringMap();
        }

        private void SliderZoom_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (!mapControl.IsMapLoaded)
            {
                return;
            }

            mapControl.Resolution = SliderZoom.Maximum - SliderZoom.Value + SliderZoom.Minimum;
            
            scaleMapModel.SliderZoomMax = SliderZoom.Maximum;
            scaleMapModel.SliderZoomMin = SliderZoom.Minimum;
            scaleMapModel.SliderZoomValue = SliderZoom.Value;
            YamlSaveScaleMap(scaleMapModel);
        }

        private void ButtonTest_Click(object sender, RoutedEventArgs e)
        {
            if (!mapControl.IsMapLoaded)
            {
                return;
            }

            //timer.Start();

            ///////////////////////////////////////////////////////////////////////////////////
            // Draw Local  Point

            Random random = new Random();
            //int CountReceivingPoint = 4;
            //List<Location> Lcoord = new List<Location>(360);
            //for (int i = 0; i < 360; i++)
            //{
            //    Coord coord = new Coord();
            //    coord.Latitude = ((MapViewModel)DataContext).LocationModel.Coordinate.Latitude + random.NextDouble() / 3;
            //    coord.Longitude = ((MapViewModel)DataContext).LocationModel.Coordinate.Longitude + random.NextDouble() / 3;
            //    Lcoord.Add(coord);
            //}

            //ReceivingPoint = new List<TableReceivingPoint>
            //{
            //    new TableReceivingPoint(){ Id=1, Coordinates=Lcoord[0], Antenna=random.Next(0,360), IsCetral=true},
            //    new TableReceivingPoint(){ Id=2, Coordinates=Lcoord[1], Antenna=random.Next(0,360), IsCetral=false},
            //    new TableReceivingPoint(){ Id=3, Coordinates=Lcoord[2], Antenna=random.Next(0,360), IsCetral=false},
            //    new TableReceivingPoint(){ Id=4, Coordinates=Lcoord[3], Antenna=random.Next(0,360), IsCetral=false}
            //};

            /////////////////////////////////////////////////////////////////////
            ZoneParams = new List<ZoneParams>
            {
                new ZoneParams(){ Radius = 100, Latitude = 27.631516, Longitude = 53.926579 },
                new ZoneParams(){ Radius = 200, Latitude = 27.633577, Longitude = 53.927086 }
            };



            //List<ZoneModel> listZoneModel = new List<ZoneModel>();
            //for (int i = 0; i < 1; i++)
            //{
            //    List<Location> Lcoord = new List<Location>(360);
            //    //Lcoord.Add(new Location(27.631516, 53.926579));
            //    //Lcoord.Add(new Location(27.633577, 53.927086));
            //    //Lcoord.Add(new Location(27.634493, 53.926273));
            //    //Lcoord.Add(new Location(27.631516, 53.926579));
            //    //DrawFeaturePolygon(TypeFeature.Polygon, Lcoord, BrushTrackUAV_Id1, PenTrackUAV_Id1, 2);

            //    for (int k = 0; k < 360; k++)
            //    {
            //        //Coord coord = new Coord();
            //        //coord.Latitude = ((MapViewModel)DataContext).LocationModel.Coordinate.Longitude + random.NextDouble() / 3;
            //        //coord.Longitude = ((MapViewModel)DataContext).LocationModel.Coordinate.Latitude + random.NextDouble() / 3;
            //        Lcoord.Add(DefinePointSphere(new Location(27.631516, 53.926579), k, 100));


            //    }

            //    DrawFeaturePolygon(TypeFeature.Polygon, Lcoord, BrushTrackUAV_Id1, PenTrackUAV_Id1, 2);
            //}



        }

        /// <summary>
        /// Timer TEST
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //public void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        //{
        //    Dispatcher.BeginInvoke(new Action(() =>
        //    {
        //        Random random = new Random();

        //    int CountDrone = random.Next(3, 6);

        //    List<Coord> Lcoord = new List<Coord>(12);
        //    for (int i = 0; i < 12; i++)
        //    {
        //        Coord coord = new Coord();
        //        coord.Latitude = ((MapViewModel)DataContext).LocationModel.Coordinate.Latitude + random.NextDouble() / 3;
        //        coord.Longitude = ((MapViewModel)DataContext).LocationModel.Coordinate.Longitude + random.NextDouble() / 3;
        //        Lcoord.Add(coord);
        //    }


        //    List<TrajectoryUAVResModel> listTrajectory = new List<TrajectoryUAVResModel>();
        //    for (int i = 0; i < CountDrone; i++)
        //    {
        //        List<TableTrajectory> track = new List<TableTrajectory>();

        //        track.Add(new TableTrajectory() { Coordinates = Lcoord[i] });
        //        int countTrack = random.Next(19, 20);
        //        for (int j = 0; j < countTrack; j++)
        //        {
        //            Coord coord = new Coord();

        //            coord.Latitude = track.Last().Coordinates.Latitude + random.NextDouble() / 10;
        //            coord.Longitude = track.Last().Coordinates.Longitude + random.NextDouble() / 10;
        //            track.Add(new TableTrajectory() { Coordinates = coord });
        //        }

        //        listTrajectory.Add(new TrajectoryUAVResModel() { Id = i + 1, Trajectories = track });

        //    }

        //    TrajectoryUAVRes = listTrajectory;
        //    }), DispatcherPriority.Background);

        //}

        private void SetZonesCenterMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (!mapControl.IsMapLoaded)
            {
                return;
            }

            IsShowZones = tbShowZones.IsChecked.Value;
            //if(tbShowZones.IsChecked.Value)
                OnUpdateZonesCenter?.Invoke(this, ((MapViewModel)DataContext).LocationModel.ClickCoord);
        }

        private void SetLocalReceptionPointMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (!mapControl.IsMapLoaded)
            {
                return;
            }

            OnSetLocalReceptionPoint?.Invoke(this, ((MapViewModel)DataContext).LocationModel.ClickCoord);
        }

        private void SetRemoteReceptionPointMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (!mapControl.IsMapLoaded)
            {
                return;
            }

            OnSetRemoteReceptionPoint?.Invoke(this, ((MapViewModel)DataContext).LocationModel.ClickCoord);
        }

        private void TButtonZones_Click(object sender, RoutedEventArgs e)
        {
            if (!mapControl.IsMapLoaded)
            {
                return;
            }

            if (tbShowZones.IsChecked.Value)
            {
                isShowZones = true;
                OnShowZones?.Invoke(this, tbShowZones.IsChecked.Value);
            }
            else
            {
                isShowZones = false;
                ClearMapZones();
            }

          
        }

        private void bClearPointsLines_Click(object sender, RoutedEventArgs e)
        {
            listLinesUAVRes = new List<TableTrajectory>();
            LinesUAVResTest = new List<ObjectDrone>();

            listPointsUAVRes = new List<ZoneParams>();
            PointsUAVResTest = new List<ZoneParams>();

            listEllipseParams = new List<EllipseParams>();
            tempLastPointTrack = new List<TableTrajectory>();
            LastPointTrack = tempLastPointTrack;
        }

        private void SetStartPointRouteMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (!mapControl.IsMapLoaded)
            {
                return;
            }
            StartPointRoute = ((MapViewModel)DataContext).LocationModel.ClickCoord;
        }

        private void SetStopPointRouteMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (!mapControl.IsMapLoaded)
            {
                return;
            }
            StopPointRoute = ((MapViewModel)DataContext).LocationModel.ClickCoord;
        }

        private void ButtonClearRoute_Click(object sender, RoutedEventArgs e)
        {
            StartPointRoute = new Location(-1, -1);
            StopPointRoute = new Location(-1, -1);
        }

        private void bOtherPoints_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!mapControl.IsMapLoaded)
                {
                    return;
                }

                // Событие удаления записей
                OnLocationGrozaS(this, null);
            }
            catch { }
        }
    }

   
}
