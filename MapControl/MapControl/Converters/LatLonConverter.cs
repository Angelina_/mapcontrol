﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace MapControl
{
    public class LatLonConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            double dLatLon = 0;
         
            try
            {
                 dLatLon = (double)value < 0 ? (double)value * -1 : (double)value;
            }
           
            catch { }

            return dLatLon.ToString("0.000000");
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value;
        }
    }
}
