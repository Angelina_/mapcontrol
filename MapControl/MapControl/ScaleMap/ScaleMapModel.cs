﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using WpfMapControl;

namespace MapControl
{ 
    class DefaultSliderZoom
    {
        public const double DefaultSliderZoomMax = 78000;
        public const double DefaultSliderZoomMin = 600;
        public const double DefaultSliderZoomValue = 600;
    }

    public class ScaleMapModel : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        #endregion

        public double SliderZoomMax { get; set; } = DefaultSliderZoom.DefaultSliderZoomMax;
        public double SliderZoomMin { get; set; } = DefaultSliderZoom.DefaultSliderZoomMin;
        public double SliderZoomValue { get; set; } = DefaultSliderZoom.DefaultSliderZoomValue;
        public double Latitude { get; set; }
        public double Longitude { get; set; }

        public ScaleMapModel() : base()
        {

        }
    }
}
