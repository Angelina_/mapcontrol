﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using YamlDotNet.Serialization;

namespace MapControl
{
    public partial class UserControlMap : UserControl
    {
        ScaleMapModel scaleMapModel = new ScaleMapModel();
        public ScaleMapModel YamlLoadScaleMap()
        {
            string text = "";
            try
            {
                using (StreamReader sr = new StreamReader(AppDomain.CurrentDomain.BaseDirectory + "ScaleMap.yaml", System.Text.Encoding.Default))
                {
                    text = sr.ReadToEnd();
                    sr.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            var deserializer = new DeserializerBuilder().Build();
            try
            {
                scaleMapModel = deserializer.Deserialize<ScaleMapModel>(text);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            if (scaleMapModel == null)
            {
                scaleMapModel = SetDefaultScaleMap();
                YamlSaveScaleMap(scaleMapModel);
            }
            return scaleMapModel;
        }

        public void YamlSaveScaleMap(ScaleMapModel scaleMapModel)
        {
            try
            {
                var serializer = new SerializerBuilder().EmitDefaults().Build();
                var yaml = serializer.Serialize(scaleMapModel);

                using (StreamWriter sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "ScaleMap.yaml", false, System.Text.Encoding.Default))
                {
                    sw.WriteLine(yaml);
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public ScaleMapModel SetDefaultScaleMap()
        {
            scaleMapModel = new ScaleMapModel();
            scaleMapModel.SliderZoomMax = DefaultSliderZoom.DefaultSliderZoomMax;
            scaleMapModel.SliderZoomMin = DefaultSliderZoom.DefaultSliderZoomMin;
            scaleMapModel.SliderZoomValue = DefaultSliderZoom.DefaultSliderZoomValue;

            YamlSaveScaleMap(scaleMapModel);

            return scaleMapModel;
        }

        public void InitScaleMap()
        {
            scaleMapModel = YamlLoadScaleMap();

            SliderZoom.Maximum = scaleMapModel.SliderZoomMax;
            SliderZoom.Minimum = scaleMapModel.SliderZoomMin;
        }

    }
}
