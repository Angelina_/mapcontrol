﻿using Bearing;
using KirasaModelsDBLib;
using Mapsui.Projection;
using Mapsui.Providers;
using Mapsui.Styles;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using WpfMapControl;

namespace MapControl
{
    public class ThreadFunc : IUAVArchiveEvent
    {
        #region Events 
        public event EventHandler<UAVArchiveEvent> OnAddTracks = (object sender, UAVArchiveEvent data) => { };
        #endregion

        TableUAVBase TableUAVBase;
        List<TableTrajectory> LTableTrajectories;

        public ThreadFunc(TableUAVBase tableUAVArchive, List<TableTrajectory> tableTrajectories)
        {
            TableUAVBase = tableUAVArchive;
            LTableTrajectories = tableTrajectories;
        }

        public void UpdateTempTrackArchive()
        {
            IEnumerable<TableTrajectory> listSortNum = LTableTrajectories.OrderBy(x => x.Num); // сортировка по Num
            List<TableTrajectory> track = listSortNum.ToList();

            for (int i = 0; i < track.Count; i++)
            {
                List<TableTrajectory> trackTemp = new List<TableTrajectory>();
                trackTemp.AddRange(track.Take(i + 1));

                OnAddTracks(this, new UAVArchiveEvent(TableUAVBase, trackTemp));

                //Console.WriteLine($"i = {i}");
                Thread.Sleep(track[i].Time.Millisecond);
            }
        }
    }

    public partial class UserControlMap : UserControl, IEventPath
    {
        public List<TableTrajectory> listLinesUAVRes = new List<TableTrajectory> { };
        public List<ZoneParams> listPointsUAVRes = new List<ZoneParams> { };
        public List<EllipseParams> listEllipseParams = new List<EllipseParams>();
        public List<TableTrajectory> tempLastPointTrack = new List<TableTrajectory>();

        #region Events 
        public event EventHandler<EventPath> OnChangeFileMap = (object sender, EventPath data) => { };
        #endregion

        #region struct Threads
        private List<SThread> lThreads = new List<SThread>(); 
        private object threadLock = new object();
        public struct SThread
        {
            public ThreadFunc ThreadFunc;
            public Thread ThreadStart;
        }
        #endregion

        /// <summary>
        /// Обновить список пунктов
        /// </summary>
        /// <param name="listReceivingPoint"></param>
        public void UpdateMapReceivingPoint(List<TableReceivingPoint> listReceivingPoint, NameTable nameTable)
        {
            if (!mapControl.IsMapLoaded)
            {
                return;
            }

            if(nameTable == NameTable.TableLocalPoints)
            {
                LocalReceivingPoint = listReceivingPoint;
            }
            if (nameTable == NameTable.TableRemotePoints)
            {
                RemoteReceivingPoint = listReceivingPoint;
            }
        }

        /// <summary>
        /// Обновить траектории ИРИ БПЛА 
        /// </summary>
        /// <param name="listUAVRes"></param>
        /// <param name="listUAVTrajectory"></param>
        public void UpdateMapTrajectories(List<TableUAVBase> listUAVRes, List<TableTrajectory> listUAVTrajectory)
        {
            if (!mapControl.IsMapLoaded)
            {
                return;
            }

            List<ObjectDrone> trajectories = new List<ObjectDrone>();

            for (int i = 0; i < listUAVRes.Count; i++)
            {
                if(listUAVRes[i].IsActive)
                { 
                    List<TableTrajectory> listTrack = listUAVTrajectory.Where(x => x.TableUAVResId == listUAVRes[i].Id).ToList();

                    IEnumerable<TableTrajectory> listSortNum = listTrack.OrderBy(x => x.Num); // сортировка по Num
                    List<TableTrajectory> track = listSortNum.ToList();

                    trajectories.Add(new ObjectDrone() { UAVRes = listUAVRes[i], Trajectories = track });
                }
            }

            TrajectoryUAVRes = trajectories;
        }

        /// <summary>
        /// Обновить траектории ИРИ БПЛА 
        /// </summary>
        /// <param name="listUAVRes"></param>
        /// <param name="listUAVTrajectory"></param>
        public void UpdateMapTrajectoriesArchive(List<TableUAVBase> listUAVRes, List<TableTrajectory> listUAVTrajectory)
        {
            if (!mapControl.IsMapLoaded)
            {
                return;
            }

            if (TrajectoryUAVResArchive != null)
            {
                for (int i = 0; i < listUAVRes.Count; i++)
                {
                    if (!listUAVRes[i].IsActive)
                    {
                        int ind = TrajectoryUAVResArchive.FindIndex(x => x.UAVRes.Id == listUAVRes[i].Id);
                        if (ind != -1)
                        {
                            int indThread = lThreads.FindIndex(x => x.ThreadStart.Name == TrajectoryUAVResArchive[ind].UAVRes.Id.ToString());
                            if (indThread != -1)
                            {
                                lThreads[indThread].ThreadFunc.OnAddTracks -= ThreadMap_OnAddTracks;
                                //lThreads[indThread].ThreadStart.Abort();
                                lThreads.RemoveAt(indThread);
                            }

                            TrajectoryUAVResArchive.RemoveAt(ind);
                        }
                    }
                }
            }

            List<ObjectDrone> trajectories = new List<ObjectDrone>();

            for (int i = 0; i < listUAVRes.Count; i++)
            {
                if (listUAVRes[i].IsActive)
                {
                    List<TableTrajectory> listTrack = listUAVTrajectory.Where(x => x.TableUAVResId == listUAVRes[i].Id).ToList();

                    IEnumerable<TableTrajectory> listSortNum = listTrack.OrderBy(x => x.Num); // сортировка по Num
                    List<TableTrajectory> track = listSortNum.ToList();

                    /////////////////////////////////////////////

                    /////////////////////////////////////////////

                    trajectories.Add(new ObjectDrone() { UAVRes = listUAVRes[i], Trajectories = track });
                }
            }

            TrajectoryUAVResArchive = trajectories;
        }

        /// <summary>
        /// Отобразить траектории ИРИ БПЛА архив
        /// </summary>
        /// <param name="listUAVRes"></param>
        /// <param name="listUAVTrajectory"></param>
        public void DisplayMapTrajectoriesArchive(List<TableUAVBase> listUAVRes, List<TableTrajectory> listUAVTrajectory)
        {
            if (!mapControl.IsMapLoaded)
            {
                return;
            }

            lThreads = new List<SThread>();
            
            for (int i = 0; i < listUAVRes.Count; i++)
            {
                List<TableTrajectory> listTrack = listUAVTrajectory.Where(x => x.TableUAVResId == listUAVRes[i].Id).ToList();

                lock (threadLock)
                {
                    SThread sThread = new SThread();
                    sThread.ThreadFunc = new ThreadFunc(listUAVRes[i], listTrack);
                    sThread.ThreadFunc.OnAddTracks += ThreadMap_OnAddTracks;
                    sThread.ThreadStart = new Thread(sThread.ThreadFunc.UpdateTempTrackArchive);
                    sThread.ThreadStart.Name = listUAVRes[i].Id.ToString();
                    sThread.ThreadStart.Start();

                    lThreads.Add(sThread);
                }
            }
        }

        /// <summary>
        /// Обновить Lines Test
        /// </summary>
        /// <param name="Lat"> широта </param>
        /// <param name="Lon"> долгота </param>
        public void UpdateMapLinesTest(double Lat, double Lon) 
        {
            try
            {
                if (!mapControl.IsMapLoaded)
                {
                    return;
                }

                List<ObjectDrone> trajectories = new List<ObjectDrone>();
                TableTrajectory line = new TableTrajectory();

                line.Coordinates.Latitude = Lat;
                line.Coordinates.Longitude = Lon;
                listLinesUAVRes.Add(line);

                if (listLinesUAVRes.Count > AmountOfDefinePoint)
                {
                    listLinesUAVRes.RemoveAt(0);
                }
                trajectories.Add(new ObjectDrone() { Trajectories = listLinesUAVRes });

                //int count = listPointsUAVRes.Count - AmountOfDefinePoint;
                //if(count > 0)
                //{
                //    IEnumerable<TableTrajectory> listTemp = listPointsUAVRes.Skip(count);

                //    trajectories.Add(new ObjectDrone() { Trajectories = listTemp.ToList() });
                //}
                //else
                //    trajectories.Add(new ObjectDrone() { Trajectories = listPointsUAVRes });

                LinesUAVResTest = trajectories;
            }
            catch { }
        }

        /// <summary>
        /// Обновить Points Test
        /// </summary>
        /// <param name="Lat"> широта </param>
        /// <param name="Lon"> долгота </param>
        public void UpdateMapPointsTest(double Lat, double Lon)
        {
            if (!mapControl.IsMapLoaded)
            {
                return;
            }

            ZoneParams point = new ZoneParams {Latitude = Lat, Longitude = Lon, Radius = 1};
            listPointsUAVRes.Add(point);

            if (listPointsUAVRes.Count > AmountOfDefinePoint)
            {
                listPointsUAVRes.RemoveAt(0);
            }

            PointsUAVResTest = listPointsUAVRes;
        }

        public List<ObjectDrone> trajectoriesArchive = new List<ObjectDrone>();
        private void ThreadMap_OnAddTracks(object sender, UAVArchiveEvent e)
        {
           try
            {
                if (TrajectoryUAVResArchive == null)
                {
                    trajectoriesArchive.Add(new ObjectDrone() { UAVRes = e.TableUAVBase, Trajectories = e.ListTracks });
                   
                    //Console.WriteLine($"trajectoriesArchive.Count_1: {trajectoriesArchive.Count}");
                }
                else
                {
                    int ind = trajectoriesArchive.FindIndex(x => x.UAVRes.Id == e.TableUAVBase.Id);
                    if(ind != -1)
                    {
                        trajectoriesArchive[ind].Trajectories = e.ListTracks;

                        //Console.WriteLine($"TrajectoryUAVResArchive.Count_2: {TrajectoryUAVResArchive.Count}");
                    }
                    else
                    {
                        trajectoriesArchive.Add(new ObjectDrone() { UAVRes = e.TableUAVBase, Trajectories = e.ListTracks });

                        //Console.WriteLine($"TrajectoryUAVResArchive.Count_3: {TrajectoryUAVResArchive.Count}");
                    }
                }
             
                TrajectoryUAVResArchive = trajectoriesArchive;
           }
           catch { }
        }

        /// <summary>
        /// Обновить траектории Аэроскоп 
        /// </summary>
        /// <param name="listTrajectories"></param>
        public void UpdateMapTrajectories(List<TableAeroscope> listAeroscope, List<TableAeroscopeTrajectory> listAeroTrajectory)
        {
            if (!mapControl.IsMapLoaded)
            {
                return;
            }

            List<ObjectDroneAero> trajectories = new List<ObjectDroneAero>();

            /////////////////////
            for (int i = 0; i < listAeroscope.Count; i++)
            {
                if (listAeroscope[i].IsActive)
                {
                    List<TableAeroscopeTrajectory> listTrack = listAeroTrajectory.Where(x => x.SerialNumber == listAeroscope[i].SerialNumber).ToList();

                    IEnumerable<TableAeroscopeTrajectory> listSortNum = listTrack.OrderBy(x => x.Num); // сортировка по Num
                    List<TableAeroscopeTrajectory> track = listSortNum.ToList();

                    trajectories.Add(new ObjectDroneAero() { UAVRes = listAeroscope[i], Trajectories = track });
                }
            }
            /////////////////////
            TrajectoryAeroscope = trajectories;
        }

        /// <summary>
        /// Обновить зоны
        /// </summary>
        /// <param name="radiusAlarm"> радиус зоны опасности </param>
        /// <param name="radiusAttention"> радиус зоны внимания </param>
        /// <param name="latitude"> Центр зоны (широта) </param>
        /// <param name="longitude"> Центр зоны (долгота) </param>
        public void UpdateMapZones(short radiusAlarm, short radiusAttention, double latitude, double longitude)
        {
            if (!mapControl.IsMapLoaded)
            {
                return;
            }

            ZoneParams = new List<ZoneParams>
            { 
                new ZoneParams
                {
                    Radius = radiusAlarm,
                    Latitude = latitude,
                    Longitude = longitude
                },
                new ZoneParams
                {
                    Radius = radiusAttention,
                    Latitude = latitude,
                    Longitude = longitude
                },
            };
        }

        /// <summary>
        /// Очистить зоны
        /// </summary>
        public void ClearMapZones()
        {
            if (!mapControl.IsMapLoaded)
            {
                return;
            }

            ZoneParams = new List<ZoneParams> { };
        }

        //private void TBChecked(bool isShowZones)
        //{
        //    tbShowZones.IsChecked = isShowZones;
        //}

        /// <summary>
        /// Тип дрона из TypeUAVRes в string
        /// </summary>
        /// <param name="typeUAVRes"></param>
        /// <returns></returns>
        private string ConvertTypeDroneUAVRes(TypeUAVRes typeUAVRes)
        {
            string sType = string.Empty;

            switch (typeUAVRes)
            {
                case TypeUAVRes.Ocusinc:
                    sType = "Mavic 2";
                    break;

                case TypeUAVRes.G3:
                    sType = "3G";
                    break;

                case TypeUAVRes.Lightbridge:
                    sType = "Phantom 3";
                    break;

                case TypeUAVRes.WiFi:
                    sType = "Mavic Air";
                    break;

                case TypeUAVRes.Unknown:
                    sType = "Unknown";
                    break;

                default:
                    sType = string.Empty;
                    break;
            }

            return sType;
        }

        /// <summary>
        /// Открыть карту
        /// </summary>
        private async void OpenDialogMap()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                try
                {
                    await mapControl.OpenMap(openFileDialog.FileName);
                    SliderZoom.Maximum = mapControl.MaxResolution;
                    SliderZoom.Minimum = mapControl.MinResolution;

                    OnChangeFileMap(this, new EventPath(openFileDialog.FileName));
                }
                catch (Exception ex)
                {
                    MessageBox.Show(string.Format("Can't load map! {0}", ex.Message));
                }
            }
        }

        /// <summary>
        /// Открыть карту
        /// </summary>
        public async void OpenMap()
        {
            try
            {
                await mapControl.OpenMap(FileMap);
                SliderZoom.Maximum = mapControl.MaxResolution;
                SliderZoom.Minimum = mapControl.MinResolution;

                SetScaleParams();

            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("Can't load map! {0}", ex.Message));
            }
        }

        /// <summary>
        /// Закрыть карту
        /// </summary>
        public void CloseMap()
        {
            mapControl.CloseMap();
        }

        /// <summary>
        /// Центрирование относительно своего местоположения
        /// </summary>
        public void CenteringMap()
        {
            try
            {
                if (!mapControl.IsMapLoaded)
                {
                    return;
                }

                if (LocalReceivingPoint.Count > 0)
                {
                    Mapsui.Geometries.Point point = ConvertToPoint(new Location(LocalReceivingPoint[0].Coordinates.Longitude, LocalReceivingPoint[0].Coordinates.Latitude));
                    mapControl.NavigateTo(point);
                }
            }
            catch { }
        }

        /// <summary>
        /// Показать / Скрыть элементы для теста
        /// </summary>
        /// <param name="isAdmin"></param>
        private void ShowElementsTest(byte isAdmin)
        {
            try
            {
                switch (isAdmin)
                {
                    case 0:
                        sClearDistance.Visibility = Visibility.Visible;
                        tbShowLinesTest.Visibility = Visibility.Visible;
                        tbShowPointsTest.Visibility = Visibility.Visible;
                        tbShowEllipseTest.Visibility = Visibility.Visible;
                        bClearPointsLines.Visibility = Visibility.Visible;
                        break;
                    case 1:
                    case 2:
                        sClearDistance.Visibility = Visibility.Hidden;
                        tbShowLinesTest.Visibility = Visibility.Hidden;
                        tbShowPointsTest.Visibility = Visibility.Hidden;
                        tbShowEllipseTest.Visibility = Visibility.Hidden;
                        bClearPointsLines.Visibility = Visibility.Hidden;

                        tbShowLinesTest.IsChecked = false;
                        tbShowPointsTest.IsChecked = false;
                        tbShowEllipseTest.IsChecked = false;

                        listLinesUAVRes = new List<TableTrajectory>();
                        listPointsUAVRes = new List<ZoneParams>();
                        listEllipseParams = new List<EllipseParams>();
                        tempLastPointTrack = new List<TableTrajectory>();
                        LastPointTrack = tempLastPointTrack;
                        break;
                    default:
                        sClearDistance.Visibility = Visibility.Hidden;
                        tbShowLinesTest.Visibility = Visibility.Hidden;
                        tbShowPointsTest.Visibility = Visibility.Hidden;
                        tbShowEllipseTest.Visibility = Visibility.Hidden;
                        bClearPointsLines.Visibility = Visibility.Hidden;

                        tbShowLinesTest.IsChecked = false;
                        tbShowPointsTest.IsChecked = false;
                        tbShowEllipseTest.IsChecked = false;

                        listLinesUAVRes = new List<TableTrajectory>();
                        listPointsUAVRes = new List<ZoneParams>();
                        listEllipseParams = new List<EllipseParams>();
                        tempLastPointTrack = new List<TableTrajectory>();
                        LastPointTrack = tempLastPointTrack;
                        break;
                }
            }
            catch { }
        }

        public void ClearListsTest()
        {
            tbShowLinesTest.IsChecked = false;
            tbShowPointsTest.IsChecked = false;
            tbShowEllipseTest.IsChecked = false;

            listLinesUAVRes = new List<TableTrajectory>();
            LinesUAVResTest = new List<ObjectDrone>();

            listPointsUAVRes = new List<ZoneParams>();
            PointsUAVResTest = new List<ZoneParams>();

            listEllipseParams = new List<EllipseParams>();
            tempLastPointTrack = new List<TableTrajectory>();
            LastPointTrack = tempLastPointTrack;
        }

        public void ClearEllipse()
        {
            tbShowEllipseTest.IsChecked = false;

            listEllipseParams = new List<EllipseParams>();
            tempLastPointTrack = new List<TableTrajectory>();
            LastPointTrack = tempLastPointTrack;
        }

        /// <summary>
        /// Рассчет расстояния от точки А до точки В
        /// </summary>
        /// <param name="Start"></param>
        /// <param name="Finish"></param>
        /// <returns></returns>
        private float CalculateDistance(Location Start, Location Finish)
        {
            float distance = 0;
            if (Start.Latitude != -1 && Start.Longitude != -1 &&
                Finish.Latitude != -1 && Finish.Longitude != -1)
                try
                {
                    distance = (float)Math.Round(ClassBearing.f_D_2Points(Start.Latitude, Start.Longitude, Finish.Latitude, Finish.Longitude, 1), 
                        MidpointRounding.AwayFromZero);
                }
                catch { }

            return distance;
        }

        /// <summary>
        /// Центрирование (пункт, источник)
        /// </summary>
        /// <param name="coord"></param>
        public void CenteringMap(Coord coord)
        {
            try
            {
                Mapsui.Geometries.Point point = ConvertToPoint(new Location(coord.Longitude, coord.Latitude));
                mapControl.NavigateTo(point);
            }
            catch { }
        }

        private IMapObject DrawObject(MapObjectStyle mapObjectStyle, Location location)
        {
            return mapControl.AddMapObject(mapObjectStyle, "", ConvertToPoint(location));
        }
       
        private IMapObject DrawObject(MapObjectStyle mapObjectStyle, string signature, Location location)
        {
            return mapControl.AddMapObject(mapObjectStyle, signature, ConvertToPoint(location));
        }
       
        private Feature DrawFeaturePolygon(TypeFeature Type, List<Location> location, Mapsui.Styles.Color fillColor, Mapsui.Styles.Pen outlinePen, double width)
        {
            Feature ifeature = new Feature();
            try
            {
                List<Mapsui.Geometries.Point> wgs84Points = new List<Mapsui.Geometries.Point>(location.Count);
               
                switch (Type)
                {
                    case TypeFeature.Polygon:

                        foreach (var l in location)
                            wgs84Points.Add(ConvertToPoint(l));

                        ifeature = mapControl.AddPolygon(wgs84Points, fillColor, outlinePen);
                        break;


                    case TypeFeature.Polyline:

                        foreach (var l in location)
                            wgs84Points.Add(ConvertToPoint(new Location(l.Latitude, l.Longitude)));

                        ifeature = mapControl.AddPolyline(wgs84Points, fillColor, width);
                        break;

                    default:
                        break;
                }

            }
            catch { return null; }

            return ifeature;
        }

        private Feature DrawFeaturePolyline(List<Location> location, Mapsui.Styles.Pen outlinePen)
        {
            Feature ifeature = new Feature();
            try
            {
                List<Mapsui.Geometries.Point> wgs84Points = new List<Mapsui.Geometries.Point>(location.Count);

                foreach (var l in location)
                    wgs84Points.Add(ConvertToPoint(new Location(l.Latitude, l.Longitude)));

                outlinePen.PenStyle = PenStyle.Dot;
                ifeature = mapControl.AddPolyline(wgs84Points, outlinePen);
                       

            }
            catch { return null; }

            return ifeature;
        }

      
        private Feature DrawFeatureRay(Location location, Mapsui.Styles.Pen outlinePen)
        {
            Feature ifeature = new Feature();
            try
            {
                Mapsui.Geometries.Point wgs84Points = new Mapsui.Geometries.Point();

                wgs84Points = ConvertToPoint(location);

                outlinePen.PenStyle = PenStyle.Dot;
                ifeature = mapControl.AddRay(wgs84Points, 0.0F);

            }
            catch { return null; }

            return ifeature;
        }

        private Feature DrawFeaturePoint(Location location, Mapsui.Styles.Pen outlinePen)
        {
            Feature ifeature = new Feature();
            try
            {
                List<Mapsui.Geometries.Point> wgs84Points = new List<Mapsui.Geometries.Point>();

                wgs84Points.Add(ConvertToPoint(location));
                wgs84Points.Add(ConvertToPoint(location));

                outlinePen.PenStyle = PenStyle.Dot;
                ifeature = mapControl.AddPolyline(wgs84Points, Color.Red, 3);

            }
            catch { return null; }

            return ifeature;
        }

        private Location DefinePointEllipse(Location locationCentre, double azimuth, double radius, double angle)
        {
            Location location = new Location();
            //Location location1 = new Location();

            try
            {
                double dLat = 0;
                double dLong = 0;

                Bearing.ClassBearing.f_Bearing(azimuth + angle, radius, 1, locationCentre.Latitude, locationCentre.Longitude, 1, ref dLong, ref dLat);

                //double dLa = dLong - locationCentre.Latitude;
                //double dLo = dLat - locationCentre.Longitude;
                //location = new Location(dLat + 0.0009, dLong + 0.0009);


                location = new Location(dLat, dLong);

                // TEST--------------------------------------------------
                // Полярные в декартовы
                //double x = radius * Math.Cos(azimuth * Math.PI / 180);
                //double y = radius * Math.Sin(azimuth * Math.PI / 180);

                ////
                //double x_ = x * Math.Cos(45 * Math.PI / 180) - y * Math.Sin(45 * Math.PI / 180) + location.Latitude;
                //double y_ = x * Math.Sin(45 * Math.PI / 180) + y * Math.Cos(45 * Math.PI / 180) + location.Longitude;
                //location1 = new Location(x_, y_);
                // --------------------------------------------------TEST
            }
            catch { }

            //return location1;
            return location;
        }

        /// <summary>
        /// Радиус эллипса для одного градуса
        /// </summary>
        /// <param name="radiusA"></param>
        /// <param name="radiusB"></param>
        /// <returns></returns>
        private double GetRadiusEllipse(Location location, float radiusA, float radiusB, int degree)
        {
            if (radiusA == 0 && radiusB == 0)
                return 0;

            double radius = (radiusA * radiusB) / Math.Sqrt(radiusB * radiusB * (Math.Cos(degree * Math.PI / 180) * Math.Cos(degree * Math.PI / 180)) +
                                                           (radiusA * radiusA * (Math.Sin(degree * Math.PI / 180) * Math.Sin(degree * Math.PI / 180))));

            return radius;
        }

        private Location DefinePointSphere(Location locationCentre, double Azimuth, double Distance)
        {

            Location locRes = new Location();
            try
            {
                double dLat = 0;
                double dLong = 0;

                Bearing.ClassBearing.f_Bearing(Azimuth, Distance, 1, locationCentre.Latitude, locationCentre.Longitude, 1, ref dLong, ref dLat);

                locRes = new Location(dLat, dLong);
            }
            catch
            { }
            return locRes;
        }
       
        private Mapsui.Geometries.Point ConvertToPoint(Location location)
        {
            Mapsui.Geometries.Point wgs84Points = new Mapsui.Geometries.Point();

            try
            {
                switch ((MapProjection)Projection)
                {
                    case MapProjection.Geo:
                        wgs84Points = location.ToPoint();

                        break;

                    case MapProjection.Mercator:

                        wgs84Points = Mercator.FromLonLat(location.ToPoint().X, location.ToPoint().Y);


                        break;

                    default:
                        break;
                }
            }
            catch { }

            return wgs84Points;
        }

        private void SetScaleParams()
        {
            try
            {
                mapControl.Resolution = scaleMapModel.SliderZoomMax - scaleMapModel.SliderZoomValue + scaleMapModel.SliderZoomMin;

            }
            catch { }
        }

      
    }
}
